<?php

namespace Claes\Test\Client;

/**
 * Test class for HttpBrowser
 */
class HttpBrowserTest extends \PHPUnit_Framework_TestCase
{
    private $CONTAINER_CLASS = '\Claes\Core\Container';
    private $DOMPARSER_CLASS = '\Claes\Crawler\DomParser';
    private $HTTP_REQUEST_CLASS = '\Claes\Crawler\HttpRequest';
    private $HTTP_REQUEST_HEADER_CLASS = '\Claes\Crawler\HttpRequestHeader';
    private $HTTP_ENGINE_CLASS = '\Claes\Crawler\CurlEngine';
    private $HTTP_RESPONSE_CLASS = '\Claes\Crawler\HttpResponse';
    private $HTTP_RESPONSE_HEADER_CLASS = '\Claes\Crawler\HttpResponseHeader';
    private $HTTP_BROWSER = '\Claes\Client\HttpBrowser';

    private $mockRequest;

    private $mockResponse;

    private $mockContainer;

    public function __construct()
    {
        $this->resetMocks();
    }

    private function resetMocks()
    {
        $mockContainer = $this->getMockBuilder($this->CONTAINER_CLASS)->setMethods(['getRequest'])->disableOriginalConstructor()->getMock();
        $mockContainer->registerRequest($this->HTTP_REQUEST_CLASS, $this->HTTP_REQUEST_HEADER_CLASS, $this->HTTP_ENGINE_CLASS);
        $mockContainer->registerResponse($this->HTTP_RESPONSE_CLASS, $this->HTTP_RESPONSE_HEADER_CLASS);
        $mockContainer->registerDomParser($this->DOMPARSER_CLASS);
        $mockRequest = $this->getMockBuilder($this->HTTP_REQUEST_CLASS)->setMethods(['send'])->disableOriginalConstructor()->getMock();
        $mockResponse = new \Claes\Crawler\HttpResponse('Ok', 200, "HTTP/1.1 201 Found\nHost: 127.0.0.1:5555\nContent-Language: fr\nSet-Cookie: some=value; path=/\nContent-type: text/html; charset=UTF-8", new \Claes\Crawler\HttpResponseHeader);
        $mockRequest->method('send')->willReturn($mockResponse);


        $mockContainer->method('getRequest')->willReturn($mockRequest);

        $mockRequest->setHeaders(new \Claes\Crawler\HttpRequestHeader());
        $mockRequest->setHttpEngine(new \Claes\Crawler\CurlEngine('http://127.0.0.1:5555'));
        $mockRequest->setContainer($mockContainer);

        $this->mockRequest = $mockRequest;
        $this->mockResponse = $mockResponse;
        $this->mockContainer = $mockContainer;
    }

    /**
     * @covers HttpBrowser::__construct()
     */
    public function testContructorSuccess()
    {
        $mockBrowser = $this->getMockBuilder($this->HTTP_BROWSER)->setMethods(['setContainer'])->getMock();

        $mockBrowser->expects($this->once())->method('setContainer');

        $reflectedClass = new \ReflectionClass($this->HTTP_BROWSER);
        $constructor = $reflectedClass->getConstructor();
        $constructor->invoke($mockBrowser);
    }

    /**
     * @covers HttpBrowser::get
     */
    public function testGet()
    {
        $httpBrowser = $this->getMockBuilder($this->HTTP_BROWSER)->setMethods(['setResponse', 'getResponse', 'setParser'])->disableOriginalConstructor()->getMock();
        $httpBrowser->setContainer($this->mockContainer);
        $httpBrowser->method('getResponse')->willReturn($this->mockResponse);

        $this->mockRequest->expects($this->once())->method('send');
        $httpBrowser->expects($this->once())->method('setResponse');
        $httpBrowser->expects($this->once())->method('setParser');

        $httpBrowser->get('http://127.0.0.1/');
    }

    /**
     * @covers HttpBrowser::get
     */
    public function testGetDelay()
    {
        $httpBrowser = $this->getMockBuilder($this->HTTP_BROWSER)->setMethods(['getFirstTime', 'getDelayMargin'])->disableOriginalConstructor()->getMock();
        $httpBrowser->setContainer($this->mockContainer);
        $httpBrowser->method('getFirstTime')->willReturn(false);
        $httpBrowser->method('getDelayMargin')->willReturn(0);

        $httpBrowser->setDelay(2);

        $start = microtime(true);
        $httpBrowser->get('http://127.0.0.1/');
        $end = microtime(true);

        $this->assertGreaterThan(2, ($end - $start));
        $this->assertLessThan(2.1, ($end - $start));
    }

    /**
     * @covers HttpBrowser::get
     */
    public function testGetDelayMargin()
    {
        $httpBrowser = $this->getMockBuilder($this->HTTP_BROWSER)->setMethods(['getFirstTime'])->disableOriginalConstructor()->getMock();
        $httpBrowser->setContainer($this->mockContainer);
        $httpBrowser->method('getFirstTime')->willReturn(false);

        $httpBrowser->setDelay(1);
        $httpBrowser->setDelayMargin(0.5);

        $start = microtime(true);
        $httpBrowser->get('http://127.0.0.1/');
        $end = microtime(true);

        $this->assertGreaterThan(0.5, ($end - $start));
        $this->assertLessThan(1.6, ($end - $start));
    }

    /**
     * @covers HttpBrowser::get
     */
    public function testGetWithoutDelay()
    {
        $httpBrowser = $this->getMockBuilder($this->HTTP_BROWSER)->setMethods(['getFirstTime'])->disableOriginalConstructor()->getMock();
        $httpBrowser->setContainer($this->mockContainer);
        $httpBrowser->method('getFirstTime')->willReturn(false);

        $httpBrowser->setDelay(0);
        $httpBrowser->setDelayMargin(0);

        $start = microtime(true);
        $httpBrowser->get('http://127.0.0.1/');
        $end = microtime(true);

        $this->assertLessThan(0.1, ($end - $start));
    }

        /**
     * @covers HttpBrowser::get
     */
    public function testGetNoDelayBecauseHostsChanged()
    {
        $httpBrowser = new \Claes\Client\HttpBrowser();
        $httpBrowser->setContainer($this->mockContainer);

        $httpBrowser->setDelay(3);
        $httpBrowser->setDelayMargin(0);

        $start = microtime(true);
        $httpBrowser->get('http://local.test.local/');
        $end = microtime(true);

        $this->assertLessThan(0.1, ($end - $start));

        $start = microtime(true);
        $httpBrowser->get('http://local.newadress.local/');
        $end = microtime(true);

        $this->assertLessThan(0.1, ($end - $start));
    }

    /**
     * @covers HttpBrowser::get
     */
    public function testGetFirstTime()
    {
        $httpBrowser = $this->getMockBuilder($this->HTTP_BROWSER)->setMethods(['getFirstTime'])->disableOriginalConstructor()->getMock();
        $httpBrowser->setContainer($this->mockContainer);
        $httpBrowser->method('getFirstTime')->willReturn(true);

        $httpBrowser->setDelay(3);
        $httpBrowser->setDelayMargin(0);

        $start = microtime(true);
        $httpBrowser->get('http://127.0.0.1/');
        $end = microtime(true);

        $this->assertLessThan(0.1, ($end - $start));
    }

    /**
     * @covers HttpBrowser::get
     */
    public function testGetFirstTimeChange()
    {
        $httpBrowser = new \Claes\Client\HttpBrowser();
        $httpBrowser->setContainer($this->mockContainer);

        $this->assertTrue($httpBrowser->getFirstTime());

        $httpBrowser->get('http://local.test.local/');

        $this->assertFalse($httpBrowser->getFirstTime());
    }

    /**
     * @covers HttpBrowser::get
     */
    public function testGetChangeHost()
    {
        $httpBrowser = new \Claes\Client\HttpBrowser();
        $httpBrowser->setContainer($this->mockContainer);

        $httpBrowser->get('http://local.test.local/');

        $this->assertSame('local.test.local', $httpBrowser->getRequest()->getHeaders()->getHost());

        $httpBrowser->get('http://local.newadress.local/');

        $this->assertSame('local.newadress.local', $httpBrowser->getRequest()->getHeaders()->getHost());
    }

    /**
     * @covers HttpBrowser::get
     */
    public function testGetNoChangeHost()
    {
        $httpBrowser = new \Claes\Client\HttpBrowser();
        $httpBrowser->setContainer($this->mockContainer);

        $httpBrowser->get('http://127.0.0.1/');

        $this->assertSame('', $httpBrowser->getRequest()->getHeaders()->getHost());

        $httpBrowser->get('http://local.newadress.local/');

        $this->assertSame('local.newadress.local', $httpBrowser->getRequest()->getHeaders()->getHost());

        $httpBrowser->get('http://192.168.1.1/');

        $this->assertSame('local.newadress.local', $httpBrowser->getRequest()->getHeaders()->getHost());
    }

    /**
     * @covers HttpBrowser::get
     */
    public function testGetWithReferer()
    {
        $httpBrowser = new \Claes\Client\HttpBrowser();
        $httpBrowser->setContainer($this->mockContainer);

        $httpBrowser->get('http://127.0.0.1/');

        $this->assertSame('http://127.0.0.1/', $httpBrowser->getRequest()->getHeaders()->getReferer());

        $httpBrowser->get('http://local.newadress.local/');

        $this->assertSame('http://local.newadress.local/', $httpBrowser->getRequest()->getHeaders()->getReferer());
    }

    /**
     * @covers HttpBrowser::get
     */
    public function testGetWithDisabledReferer()
    {
        $httpBrowser = new \Claes\Client\HttpBrowser();
        $httpBrowser->setContainer($this->mockContainer);
        $httpBrowser->setDisableReferer(true);

        $httpBrowser->get('http://127.0.0.1/');

        $this->assertSame('', $httpBrowser->getRequest()->getHeaders()->getReferer());

        $httpBrowser->get('http://local.newadress.local/');

        $this->assertSame('', $httpBrowser->getRequest()->getHeaders()->getReferer());
    }

    /**
     * @covers HttpBrowser::get
     */
    public function testGetErrorWithStrictMode()
    {
        $mockRequest = $this->getMockBuilder($this->HTTP_REQUEST_CLASS)->setMethods(['send'])->disableOriginalConstructor()->getMock();
        $mockContainer = $this->getMockBuilder($this->CONTAINER_CLASS)->setMethods(['getRequest'])->disableOriginalConstructor()->getMock();
        $mockContainer->registerRequest($this->HTTP_REQUEST_CLASS, $this->HTTP_REQUEST_HEADER_CLASS, $this->HTTP_ENGINE_CLASS);
        $mockContainer->registerResponse($this->HTTP_RESPONSE_CLASS, $this->HTTP_RESPONSE_HEADER_CLASS);
        $mockContainer->registerDomParser($this->DOMPARSER_CLASS);
        $mockRequest = $this->getMockBuilder($this->HTTP_REQUEST_CLASS)->setMethods(['send'])->disableOriginalConstructor()->getMock();
        $mockResponse = new \Claes\Crawler\HttpResponse('NOT FOUND', 404, "HTTP/1.1 404 Not Found\nHost: 127.0.0.1:5555\nContent-Language: fr\nContent-type: text/html; charset=UTF-8", new \Claes\Crawler\HttpResponseHeader());
        $mockRequest->method('send')->willReturn($mockResponse);


        $mockContainer->method('getRequest')->willReturn($mockRequest);

        $mockRequest->setHeaders(new \Claes\Crawler\HttpRequestHeader());
        $mockRequest->setHttpEngine(new \Claes\Crawler\CurlEngine('http://127.0.0.1:5555'));
        $mockRequest->setContainer($mockContainer);

        $this->mockRequest = $mockRequest;
        $this->mockResponse = $mockResponse;
        $this->mockContainer = $mockContainer;

        $httpBrowser = new \Claes\Client\HttpBrowser();
        $httpBrowser->setContainer($this->mockContainer);
        $httpBrowser->setStrictMode(true);

        $this->setExpectedExceptionRegExp('Claes\Exception\ResponseException', '#.*http\:\/\/127\.0\.0\.1\/.*404.*#');

        $httpBrowser->get('http://127.0.0.1/');

        $this->resetMocks();
    }

    /**
     * @covers HttpBrowser::get
     */
    public function testGetErrorWithoutStrictMode()
    {
        $mockRequest = $this->getMockBuilder($this->HTTP_REQUEST_CLASS)->setMethods(['send'])->disableOriginalConstructor()->getMock();
        $mockContainer = $this->getMockBuilder($this->CONTAINER_CLASS)->setMethods(['getRequest'])->disableOriginalConstructor()->getMock();
        $mockContainer->registerRequest($this->HTTP_REQUEST_CLASS, $this->HTTP_REQUEST_HEADER_CLASS, $this->HTTP_ENGINE_CLASS);
        $mockContainer->registerResponse($this->HTTP_RESPONSE_CLASS, $this->HTTP_RESPONSE_HEADER_CLASS);
        $mockContainer->registerDomParser($this->DOMPARSER_CLASS);
        $mockRequest = $this->getMockBuilder($this->HTTP_REQUEST_CLASS)->setMethods(['send'])->disableOriginalConstructor()->getMock();
        $mockResponse = new \Claes\Crawler\HttpResponse('Server fails', 500, "HTTP/1.1 500 Server fails!\nHost: 127.0.0.1:5555\nContent-Language: fr\nContent-type: text/html; charset=UTF-8", new \Claes\Crawler\HttpResponseHeader());
        $mockRequest->method('send')->willReturn($mockResponse);

        $mockContainer->method('getRequest')->willReturn($mockRequest);

        $mockRequest->setHeaders(new \Claes\Crawler\HttpRequestHeader());
        $mockRequest->setHttpEngine(new \Claes\Crawler\CurlEngine('http://127.0.0.1:5555'));
        $mockRequest->setContainer($mockContainer);

        $this->mockRequest = $mockRequest;
        $this->mockResponse = $mockResponse;
        $this->mockContainer = $mockContainer;

        $httpBrowser = new \Claes\Client\HttpBrowser();
        $httpBrowser->setContainer($this->mockContainer);
        $httpBrowser->setStrictMode(false);

        $httpBrowser->get('http://127.0.0.1/');

        $this->resetMocks();
    }

    /**
     * @covers HttpBrowser::get
     */
    public function testGetCookies()
    {
        $httpBrowser = new \Claes\Client\HttpBrowser();
        $httpBrowser->setContainer($this->mockContainer);

        $httpBrowser->get('http://127.0.0.1/');

        $this->assertSame('some=value; path=%2F', $httpBrowser->getRequest()->getHeaders()->getCookie());
    }
    /**
     * @covers HttpBrowser::get
     */
    public function testGetCookiesConsecutive()
    {
        $httpBrowser = new \Claes\Client\HttpBrowser();
        $httpBrowser->setContainer($this->mockContainer);

        $httpBrowser->get('http://127.0.0.1/');

        $httpBrowser->getRequest()->getHeaders()->setCookie('existing=true');

        $httpBrowser->get('http://127.0.0.1/');

        $this->assertSame('some=value; path=%2F; existing=true', $httpBrowser->getRequest()->getHeaders()->getCookie());
    }

    /**
     * @covers HttpBrowser::getRequest()
     * @covers HttpBrowser::setRequest()
     */
    public function testAccessorRequest()
    {
        $browser = new \Claes\Client\HttpBrowser();
        $browser->setRequest(new \Claes\Crawler\HttpRequest(new \Claes\Crawler\HttpRequestHeader(), new \Claes\Crawler\CurlEngine('http://192.168.1.1'), $this->mockContainer));
        $this->assertTrue(is_subclass_of($browser->getRequest(), 'Claes\Crawler\Interfaces\HttpRequestInterface'));
        $firstId = spl_object_hash($browser->getRequest());

        $newObject = new \Claes\Crawler\HttpRequest(new \Claes\Crawler\HttpRequestHeader(), new \Claes\Crawler\CurlEngine('http://192.168.1.1'), $this->mockContainer);
        $newId = spl_object_hash($newObject);
        $browser->setRequest($newObject);

        $this->assertTrue(is_subclass_of($browser->getRequest(), 'Claes\Crawler\Interfaces\HttpRequestInterface'));
        $this->assertNotEquals($firstId, spl_object_hash($browser->getRequest()));
        $this->assertSame($newId, spl_object_hash($browser->getRequest()));
    }

    /**
     * @covers HttpBrowser::getResponse()
     * @covers HttpBrowser::setResponse()
     */
    public function testAccessorResponse()
    {
        $browser = new \Claes\Client\HttpBrowser();
        $browser->setResponse(new \Claes\Crawler\HttpResponse('Ok', 200, "HTTP/1.1 201 Found\nHost: 127.0.0.1:5555\nContent-Language: fr\nContent-type: text/html; charset=UTF-8", new \Claes\Crawler\HttpResponseHeader));
        $this->assertTrue(is_subclass_of($browser->getResponse(), 'Claes\Crawler\Interfaces\HttpResponseInterface'));
        $firstId = spl_object_hash($browser->getResponse());

        $newObject = new \Claes\Crawler\HttpResponse('Ok', 200, "HTTP/1.1 201 Found\nHost: 127.0.0.1:5555\nContent-Language: fr\nContent-type: text/html; charset=UTF-8", new \Claes\Crawler\HttpResponseHeader);
        $newId = spl_object_hash($newObject);
        $browser->setResponse($newObject);

        $this->assertTrue(is_subclass_of($browser->getResponse(), 'Claes\Crawler\Interfaces\HttpResponseInterface'));
        $this->assertNotEquals($firstId, spl_object_hash($browser->getResponse()));
        $this->assertSame($newId, spl_object_hash($browser->getResponse()));
    }

    /**
     * @covers HttpBrowser::getParser()
     * @covers HttpBrowser::setParser()
     */
    public function testAccessorParser()
    {
        $browser = new \Claes\Client\HttpBrowser();
        $browser->setParser(new \Claes\Crawler\DomParser('Ok'));
        $this->assertTrue(is_subclass_of($browser->getParser(), 'Claes\Crawler\Interfaces\DomParserInterface'));
        $firstId = spl_object_hash($browser->getParser());

        $newObject = new \Claes\Crawler\DomParser('Ok');
        $newId = spl_object_hash($newObject);
        $browser->setParser($newObject);

        $this->assertTrue(is_subclass_of($browser->getParser(), 'Claes\Crawler\Interfaces\DomParserInterface'));
        $this->assertNotEquals($firstId, spl_object_hash($browser->getParser()));
        $this->assertSame($newId, spl_object_hash($browser->getParser()));
    }

    /**
     * @covers HttpBrowser::getContainer()
     * @covers HttpBrowser::setContainer()
     */
    public function testAccessorContainer()
    {
        $browser = new \Claes\Client\HttpBrowser();
        $browser->setContainer(new \Claes\Core\Container());
        $this->assertTrue(is_subclass_of($browser->getContainer(), 'Claes\Core\Interfaces\ContainerInterface'));
        $firstId = spl_object_hash($browser->getContainer());

        $newObject = new \Claes\Core\Container();
        $newId = spl_object_hash($newObject);
        $browser->setContainer($newObject);

        $this->assertTrue(is_subclass_of($browser->getContainer(), 'Claes\Core\Interfaces\ContainerInterface'));
        $this->assertNotEquals($firstId, spl_object_hash($browser->getContainer()));
        $this->assertSame($newId, spl_object_hash($browser->getContainer()));
    }

    /**
     * @covers HttpRequest::getDelay()
     * @covers HttpRequest::setDelay()
     */
    public function testAccessorDelay()
    {
        $browser = new \Claes\Client\HttpBrowser();
        $this->assertSame(0, $browser->getDelay());

        $browser->setDelay(7);
        $this->assertSame(7, $browser->getDelay());
    }

    /**
     * @covers HttpRequest::getDelayMargin()
     * @covers HttpRequest::getDelayMargin()
     */
    public function testAccessorDelayMargin()
    {
        $browser = new \Claes\Client\HttpBrowser();
        $this->assertSame(0, $browser->getDelayMargin());

        $browser->setDelayMargin(3);
        $this->assertSame(3, $browser->getDelayMargin());
    }

    /**
     * @covers HttpRequest::getDisableReferer()
     * @covers HttpRequest::getDisableReferer()
     */
    public function testAccessorDisableReferer()
    {
        $browser = new \Claes\Client\HttpBrowser();
        $this->assertSame(false, $browser->getDisableReferer());

        $browser->setDisableReferer(true);
        $this->assertSame(true, $browser->getDisableReferer());
    }

    /**
     * @covers HttpRequest::getStrictMode()
     * @covers HttpRequest::getStrictMode()
     */
    public function testAccessorStrictMode()
    {
        $browser = new \Claes\Client\HttpBrowser(); 
        $this->assertSame(true, $browser->getStrictMode());

        $browser->setStrictMode(false);
        $this->assertSame(false, $browser->getStrictMode());
    }

    /**
     * @covers HttpRequest::getFirstTime()
     * @covers HttpRequest::getFirstTime()
     */
    public function testAccessorFirstTime()
    {
        $browser = new \Claes\Client\HttpBrowser();
        $this->assertSame(true, $browser->getFirstTime());

        $browser->setFirstTime(false);
        $this->assertSame(false, $browser->getFirstTime());
    }
}
