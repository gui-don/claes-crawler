<?php

namespace Claes\Test\Client;

use Claes\Test\Crawler\HttpRequestTest;

/**
 * Test class for FirefoxRequestWrapper
 */
class FirefoxRequestWrapperTest extends HttpRequestTest
{
    protected $HTTP_REQUEST_CLASS = '\Claes\Client\FirefoxRequestWrapper';

    /**
     * @covers HttpRequest::__construct
     */
    public function testContructor()
    {
        $mock = $this->getMockBuilder($this->HTTP_REQUEST_CLASS)->setMethods(['send'])->disableOriginalConstructor()->getMock();
        $requestHeader = new $this->HTTP_REQUEST_HEADER_CLASS();
        $httpEngine = new $this->HTTP_ENGINE_CLASS('http://127.0.0.1');

        $reflectedClass = new \ReflectionClass($this->HTTP_REQUEST_CLASS);
        $constructor = $reflectedClass->getConstructor();
        $constructor->invoke($mock, $requestHeader, $httpEngine, $this->container);

        $headers = $mock->getHeaders()->convertCurl();
        $this->assertEquals('Host: 127.0.0.1', $headers[0]);
        $this->assertEquals('User-Agent: Mozilla/5.0 (Windows NT 6.1; rv:44.0) Gecko/20100101 Firefox/44.0', $headers[1]);
        $this->assertEquals('Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8', $headers[2]);
        $this->assertEquals('Accept-Encoding: gzip, deflate', $headers[3]);
        $this->assertEquals('DNT: 1', $headers[4]);
        $this->assertEquals('Connection: keep-alive', $headers[5]);
        $this->assertFalse(isset($headers[6]));
    }

    /**
     * @covers HttpRequest::organizeHeaders
     */
    public function testOrganizeHeaders()
    {
        $this->resetMocks();
        $this->container->getRequest('http://127.0.0.1')->getHeaders()->setAcceptEncoding('gzip, deflate');
        $this->container->getRequest('http://127.0.0.1')->getHeaders()->setCacheControl('max-age=0');
        $this->container->getRequest('http://127.0.0.1')->getHeaders()->setConnection('keep-alive');
        $this->container->getRequest('http://127.0.0.1')->getHeaders()->setHost('127.0.0.1');
        $this->container->getRequest('http://127.0.0.1')->getHeaders()->setAccept('text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8');
        $this->container->getRequest('http://127.0.0.1')->getHeaders()->setDnt(true);
        $this->container->getRequest('http://127.0.0.1')->getHeaders()->setUserAgent('Mozilla/5.0 (Windows NT 6.1; rv:44.0) Gecko/20100101 Firefox/44.0');

        $headers = $this->container->getRequest('http://127.0.0.1')->getHeaders()->convertCurl();
        $this->assertEquals('Accept-Encoding: gzip, deflate', $headers[0]);
        $this->assertEquals('Cache-Control: max-age=0', $headers[1]);
        $this->assertEquals('Connection: keep-alive', $headers[2]);
        $this->assertEquals('Host: 127.0.0.1', $headers[3]);
        $this->assertEquals('Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8', $headers[4]);
        $this->assertEquals('DNT: 1', $headers[5]);
        $this->assertEquals('User-Agent: Mozilla/5.0 (Windows NT 6.1; rv:44.0) Gecko/20100101 Firefox/44.0', $headers[6]);
        $this->assertFalse(isset($headers[7]));

        $this->container->getRequest('http://127.0.0.1')->organizeHeaders();

        $headers = $this->container->getRequest('http://127.0.0.1')->getHeaders()->convertCurl();
        $this->assertEquals('Host: 127.0.0.1', $headers[0]);
        $this->assertEquals('User-Agent: Mozilla/5.0 (Windows NT 6.1; rv:44.0) Gecko/20100101 Firefox/44.0', $headers[1]);
        $this->assertEquals('Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8', $headers[2]);
        $this->assertEquals('Accept-Encoding: gzip, deflate', $headers[3]);
        $this->assertEquals('DNT: 1', $headers[4]);
        $this->assertEquals('Connection: keep-alive', $headers[5]);
        $this->assertEquals('Cache-Control: max-age=0', $headers[6]);
        $this->assertFalse(isset($headers[7]));
    }

    /**
     * @covers HttpRequest::send
     */
    public function testSend()
    {
        $this->mockHttpEngine = $this->getMockBuilder($this->HTTP_ENGINE_CLASS)->setMethods(['get', 'setHeaders'])->setConstructorArgs(['http://127.0.0.1'])->getMock();
        $this->container->getRequest('http://127.0.0.1')->getHeaders()->setCacheControl('max-age=0');
        $this->container->getRequest('http://127.0.0.1')->getHeaders()->setAcceptLanguage('en_US');
        $this->container->getRequest('http://127.0.0.1')->organizeHeaders();

        $this->mockHttpEngine->expects($this->once())->method('get');
        $this->mockHttpEngine->expects($this->once())->method('setHeaders')->with($this->equalTo($this->container->getRequest('http://127.0.0.1')->getHeaders()->convertCurl()));
        $this->resetMocks();

        $httpRequest = $this->container->getRequest('http://127.0.0.1');
        $httpRequest->getHeaders()->setCacheControl('max-age=0');
        $httpRequest->getHeaders()->setAcceptLanguage('en_US');

        $httpRequest->send();

        $headers = $httpRequest->getHeaders()->convertCurl();
        $this->assertEquals('Host: 127.0.0.1', $headers[0]);
        $this->assertEquals('User-Agent: Mozilla/5.0 (Windows NT 6.1; rv:44.0) Gecko/20100101 Firefox/44.0', $headers[1]);
        $this->assertEquals('Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8', $headers[2]);
        $this->assertEquals('Accept-Language: en_US', $headers[3]);
        $this->assertEquals('Accept-Encoding: gzip, deflate', $headers[4]);
        $this->assertEquals('DNT: 1', $headers[5]);
        $this->assertEquals('Connection: keep-alive', $headers[6]);
        $this->assertEquals('Cache-Control: max-age=0', $headers[7]);
        $this->assertFalse(isset($headers[8]));
    }
}
