<?php

namespace Claes\Test\Client;

/**
 * Test class for HttpBrowser
 */
class FirefoxBrowserTest extends \PHPUnit_Framework_TestCase
{
    protected $HTTP_BROWSER = '\Claes\Client\FirefoxBrowser';


    /**
     * @covers HttpBrowser::__construct()
     */
    public function testContructorSuccess()
    {
        $mockBrowser = $this->getMockBuilder($this->HTTP_BROWSER)->setMethods(['setContainer'])->getMock();

        $mockBrowser->expects($this->once())->method('setContainer');

        $reflectedClass = new \ReflectionClass($this->HTTP_BROWSER);
        $constructor = $reflectedClass->getConstructor();
        $constructor->invoke($mockBrowser);
    }
}
