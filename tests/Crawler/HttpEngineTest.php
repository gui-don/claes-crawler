<?php

namespace Claes\Test\Crawler;

/**
 * Test class for HttpEngine
 */
class HttpEngineTest extends \PHPUnit_Framework_TestCase
{
    private $classname = '\Claes\Crawler\CurlEngine';

    public static $webServerProcess;
    public static $pipes;

    public static function setUpBeforeClass()
    {
        $descriptorspec = [
            0 => ["pipe", "r"],
            1 => ["pipe", "w"],
        ];

        self::$webServerProcess = proc_open('php -S 127.0.0.1:5555', $descriptorspec, self::$pipes, __DIR__.'/files');
        sleep(1);
    }

    public static function tearDownAfterClass()
    {
        fclose(self::$pipes[0]);
        fclose(self::$pipes[1]);

        proc_terminate(self::$webServerProcess);
    }

    public function providerConstruct()
    {
        return [
            ['http://127.0.0.1'], // 0
            ['https://mylittlepony.org:8888'],
            ['https://fr.wikipedia.org/wiki/Pokémon'],
        ];
    }

    public function providerConstructFail()
    {
        return [
            [3], // 0
            ['wrong URL'],
            ['23.345.234.12'],
            ['http://ok'],
            ['345'],
            [false], // 5
            [null],
        ];
    }

    /**
     * @covers HttpEngine::__construct()
     * @dataProvider providerConstruct
     */
    public function testConstruct($url)
    {
        $httpEngine = new $this->classname($url);
        $this->assertSame($url, $httpEngine->getTransactionInformation()['effective_url']);
    }

    /**
     * @covers HttpEngine::__construct()
     * @dataProvider providerConstructFail
     */
    public function testConstructFail($url)
    {
        $this->setExpectedExceptionRegExp('Claes\Exception\CurlUrlException', '#'.$url.'#');
        new $this->classname($url);
    }

    /**
     * @covers HttpEngine::__destruct()
     */
    public function testDestruct()
    {
        $httpEngine = new $this->classname('https://fr.wikipedia.org/wiki/Pokémon');

        $this->assertSame('resource', gettype($httpEngine->getCurlResource()));

        $httpEngine->__destruct();

        $this->assertSame('unknown type', gettype($httpEngine->getCurlResource()));

        $httpEngine->__construct('https://fr.wikipedia.org/wiki/Pokémon');

        $this->assertSame('resource', gettype($httpEngine->getCurlResource()));
    }

    /**
     * @covers HttpEngine::__toString()
     */
    public function testToString()
    {
        $httpEngine = new $this->classname('https://fr.wikipedia.org/wiki/Pokémon');

        $this->assertRegExp('#https://fr.wikipedia.org/wiki/Pokémon#', $httpEngine->__toString());
    }

    /**
     * @covers HttpEngine::get()
     */
    public function testGet()
    {
        $httpEngine = new $this->classname('http://127.0.0.1/httpAnswer.php');

        $httpEngine->setPort(5555);
        $httpEngine->get();

        $this->assertRegexp("#HTTP/1.1 200 OK#", $httpEngine->getResponseHeaders());
        $this->assertRegexp("#Set-Cookie: whatwhat=ok#", $httpEngine->getResponseHeaders());
        $this->assertRegexp("#Content-Language: fr#", $httpEngine->getResponseHeaders());
        $this->assertSame('TOUT EST BON', $httpEngine->getResponseBody());
    }

    /**
     * @covers HttpEngine::get()
     */
    public function testGetReplay()
    {
        $httpEngine = new $this->classname('http://127.0.0.1/httpAnswer.php');

        $httpEngine->setPort(5555);
        $httpEngine->setFollowRedirections(true);
        $httpEngine->get();

        $this->assertRegexp("#HTTP/1.1 200 OK#", $httpEngine->getResponseHeaders());
        $this->assertRegexp("#Set-Cookie: whatwhat=ok#", $httpEngine->getResponseHeaders());
        $this->assertRegexp("#Content-Language: fr#", $httpEngine->getResponseHeaders());
        $this->assertSame('TOUT EST BON', $httpEngine->getResponseBody());

        $httpEngine->setFollowRedirections(false);
        $httpEngine->setUrl('http://127.0.0.1/nothing');

        $httpEngine->get();

        $this->assertRegexp("#HTTP/1.1 404 Not Found#", $httpEngine->getResponseHeaders());
        $this->assertRegexp("#Not Found#", $httpEngine->getResponseBody());

        $httpEngine->setUrl('http://127.0.0.1/redirect302.php');
        $httpEngine->get();

        $this->assertSame('', $httpEngine->getResponseBody());
        $this->assertRegexp("#HTTP/1.1 302 Found#", $httpEngine->getResponseHeaders());
        $this->assertRegexp("#Status: HTTP/1.1 302 Moved Temporarly#", $httpEngine->getResponseHeaders());
        $this->assertRegexp("#Set-Cookie: redirection=from302#", $httpEngine->getResponseHeaders());
        $this->assertRegexp("#Location: httpAnswer.php#", $httpEngine->getResponseHeaders());
    }

    /**
     * @covers HttpEngine::get()
     */
    public function testGetRedirect()
    {
        $httpEngine = new $this->classname('http://127.0.0.1/redirect302.php');

        $httpEngine->setPort(5555);
        $httpEngine->setFollowRedirections(true);
        $httpEngine->get();

        $this->assertRegexp('#HTTP/1.1 200 OK#', $httpEngine->getResponseHeaders());
        $this->assertRegexp('#HTTP/1.1 302 Found#', $httpEngine->getResponseHeaders());
        $this->assertRegexp('#Host: 127.0.0.1:5555#', $httpEngine->getResponseHeaders());
        $this->assertRegexp('#Connection: close#', $httpEngine->getResponseHeaders());
        $this->assertRegexp('#Set-Cookie: redirection=from302#', $httpEngine->getResponseHeaders());
        $this->assertRegexp('#Set-Cookie: whatwhat=ok#', $httpEngine->getResponseHeaders());
        $this->assertRegexp('#Content-Language: fr#', $httpEngine->getResponseHeaders());
        $this->assertRegexp('#Content-type: text/html; charset=UTF-8#', $httpEngine->getResponseHeaders());
        $this->assertRegexp('#Status: HTTP/1.1 302 Moved Temporarly#', $httpEngine->getResponseHeaders());
    }

    /**
     * @covers HttpEngine::get()
     * @expectedException Claes\Exception\CurlGetException
     */
    public function testGetFail()
    {
        $httpEngine = new $this->classname('http://127.0.0.1/redirect302.php');
        $httpEngine->get();
    }

    /**
     * @covers HttpEngine::get()
     * @expectedException Claes\Exception\CurlUrlException
     */
    public function testGetFailWrongUrl()
    {
        $httpEngine = new $this->classname('http://127.0.0.1/redirect302.php');
        $httpEngine->setUrl('this is wrong');
        $httpEngine->get();
    }

    /**
     * @covers HttpEngine::getResponseHeaders()
     */
    public function testGetResponseHeaders()
    {
        $httpEngine = new $this->classname('http://127.0.0.1:5555/httpAnswer.php');
        $this->assertSame('', $httpEngine->getResponseHeaders());

        $httpEngine->get();

        $this->assertRegexp("#HTTP/1.1 200 OK#", $httpEngine->getResponseHeaders());
        $this->assertRegexp("#Set-Cookie: whatwhat=ok#", $httpEngine->getResponseHeaders());
        $this->assertRegexp("#Content-Language: fr#", $httpEngine->getResponseHeaders());
    }

    /**
     * @covers HttpEngine::getResponseBody()
     */
    public function testGetResponseBody()
    {
        $httpEngine = new $this->classname('http://127.0.0.1:5555/httpAnswer.php');
        $this->assertSame('', $httpEngine->getResponseBody());

        $httpEngine->get();

        $this->assertSame('TOUT EST BON', $httpEngine->getResponseBody());
    }

    /**
     * @covers HttpEngine::getResponseBody()
     */
    public function testGetResponseCode()
    {
        $httpEngine = new $this->classname('http://127.0.0.1:5555/httpAnswer.php');
        $httpEngine->get();
        $this->assertSame(200, $httpEngine->getResponseCode());

        $httpEngine->setUrl('http://127.0.0.1:5555/404');
        $httpEngine->get();
        $this->assertSame(404, $httpEngine->getResponseCode());
    }

    /**
     * @covers HttpEngine::getTransactionInformation()
     */
    public function testGetTransactionInformation()
    {
        $httpEngine = new $this->classname('http://127.0.0.1:5555/server.php');

        $httpEngine->get();

        $this->assertSame('http://127.0.0.1:5555/server.php', $httpEngine->getTransactionInformation()['effective_url']);
        $this->assertSame(200, $httpEngine->getTransactionInformation()['http_code']);
        $this->assertSame(-1, $httpEngine->getTransactionInformation()['filetime']);
        $this->assertGreaterThan(0, $httpEngine->getTransactionInformation()['total_time']);
        $this->assertSame(0, $httpEngine->getTransactionInformation()['redirect_count']);
        $this->assertSame(0.0, $httpEngine->getTransactionInformation()['redirect_time']);
        $this->assertSame(false, $httpEngine->getTransactionInformation()['redirect_url']);
        $this->assertSame('127.0.0.1', $httpEngine->getTransactionInformation()['primary_ip']);
        $this->assertSame(5555, $httpEngine->getTransactionInformation()['primary_port']);
        $this->assertSame('127.0.0.1', $httpEngine->getTransactionInformation()['local_ip']);
        $this->assertRegexp("#GET /server.php HTTP/1.1#", $httpEngine->getTransactionInformation()['header_out']);
        $this->assertRegexp("#Host: 127.0.0.1:5555#", $httpEngine->getTransactionInformation()['header_out']);
        $this->assertRegexp("#Accept: */*#", $httpEngine->getTransactionInformation()['header_out']);
    }

    /**
     * @covers HttpEngine::getTransactionInformation()
     */
    public function testGetTransactionInformationRedirect()
    {
        $httpEngine = new $this->classname('http://127.0.0.1:5555/redirect301.php');

        $httpEngine->setFollowRedirections(true);
        $httpEngine->get();

        $this->assertSame('http://127.0.0.1:5555/httpAnswer.php', $httpEngine->getTransactionInformation()['effective_url']);
        $this->assertSame(200, $httpEngine->getTransactionInformation()['http_code']);
        $this->assertSame(1, $httpEngine->getTransactionInformation()['redirect_count']);
        $this->assertGreaterThan(0, $httpEngine->getTransactionInformation()['redirect_time']);
        $this->assertSame(false, $httpEngine->getTransactionInformation()['redirect_url']);
        $this->assertRegexp("#GET /httpAnswer.php HTTP/1.1#", $httpEngine->getTransactionInformation()['header_out']);
    }

    /**
     * @covers HttpEngine::getTransactionInformation()
     */
    public function testGetTransactionInformationNoRedirect()
    {
        $httpEngine = new $this->classname('http://127.0.0.1:5555/redirect301.php');

        $httpEngine->setFollowRedirections(false);
        $httpEngine->get();

        $this->assertSame('http://127.0.0.1:5555/redirect301.php', $httpEngine->getTransactionInformation()['effective_url']);
        $this->assertSame(0, $httpEngine->getTransactionInformation()['redirect_count']);
        $this->assertSame(0.0, $httpEngine->getTransactionInformation()['redirect_time']);
        $this->assertSame('http://127.0.0.1:5555/httpAnswer.php', $httpEngine->getTransactionInformation()['redirect_url']);
        $this->assertRegexp("#GET /redirect301.php HTTP/1.1#", $httpEngine->getTransactionInformation()['header_out']);
    }

    /**
     * @covers HttpEngine::getTransactionInformation()
     */
    public function testGetTransactionInformationFail()
    {
        $httpEngine = new $this->classname('http://127.0.0.1:5555/nothing');

        $httpEngine->get();

        $this->assertSame('http://127.0.0.1:5555/nothing', $httpEngine->getTransactionInformation()['effective_url']);
        $this->assertSame(404, $httpEngine->getTransactionInformation()['http_code']);
        $this->assertRegexp("#GET /nothing HTTP/1.1#", $httpEngine->getTransactionInformation()['header_out']);
    }

    /**
     * @covers HttpEngine::getCurlResource()
     */
    public function testGetCurlResource()
    {
        $httpEngine = new $this->classname('http://127.0.0.1:5555/redirect301.php');

        $this->assertSame('resource', gettype($httpEngine->getCurlResource()));
    }

    /**
     * @covers HttpEngine::getFollowRedirections()
     * @covers HttpEngine::setFollowRedirections()
     */
    public function testAccessorFollowRedirections()
    {
        $httpEngine = new $this->classname('http://127.0.0.1:5555/redirect301.php');
        $this->assertSame(true, $httpEngine->getFollowRedirections());

        $httpEngine->setFollowRedirections(false);
        $this->assertSame(false, $httpEngine->getFollowRedirections());

        $httpEngine->setFollowRedirections(true);
        $this->assertSame(true, $httpEngine->getFollowRedirections());
    }

    /**
     * @covers HttpEngine::getUrl()
     * @covers HttpEngine::setUrl()
     */
    public function testAccessorUrl()
    {
        $httpEngine = new $this->classname('http://127.0.0.1/redirect301.php');
        $this->assertSame('http://127.0.0.1/redirect301.php', $httpEngine->getUrl());

        $httpEngine->setUrl('https://www.jeuxvideo.com/');
        $this->assertSame('https://www.jeuxvideo.com/', $httpEngine->getUrl());
    }

    /**
     * @covers HttpEngine::setUrl()
     * @expectedException Claes\Exception\CurlUrlException
     */
    public function testUrlFail()
    {
        $httpEngine = new $this->classname('http://127.0.0.1/redirect301.php');

        $httpEngine->setUrl('Wrong URL');
    }

    /**
     * @covers HttpEngine::getHttpVersion()
     * @covers HttpEngine::setHttpVersion()
     */
    public function testAccessorHttpVersion()
    {
        $httpEngine = new $this->classname('http://127.0.0.1/redirect301.php');
        $this->assertSame(CURL_HTTP_VERSION_1_1, $httpEngine->getHttpVersion());

        $httpEngine->setHttpVersion(CURL_HTTP_VERSION_1_0);
        $this->assertSame(CURL_HTTP_VERSION_1_0, $httpEngine->getHttpVersion());
    }

    /**
     * @covers HttpEngine::getHttpVersion()
     * @covers HttpEngine::setHttpVersion()
     */
    public function testAccessorSslVersion()
    {
        $httpEngine = new $this->classname('http://127.0.0.1/redirect301.php');
        $this->assertSame(CURL_SSLVERSION_TLSv1_2, $httpEngine->getSslVersion());

        $httpEngine->setSslVersion(CURL_SSLVERSION_TLSv1_0);
        $this->assertSame(CURL_SSLVERSION_TLSv1_0, $httpEngine->getSslVersion());
    }

    /**
     * @covers HttpEngine::getHttpVerify()
     * @covers HttpEngine::setHttpVerify()
     */
    public function testAccessorSslVerify()
    {
        $httpEngine = new $this->classname('http://127.0.0.1/redirect301.php');
        $this->assertSame(true, $httpEngine->getSslVerify());

        $httpEngine->setSslVerify(false);
        $this->assertSame(false, $httpEngine->getSslVerify());
    }

    /**
     * @covers HttpEngine::getPort()
     * @covers HttpEngine::setPort()
     */
    public function testAccessorPort()
    {
        $httpEngine = new $this->classname('http://127.0.0.1/redirect301.php');
        $this->assertSame(null, $httpEngine->getPort());

        $httpEngine->setPort(1111);
        $this->assertSame(1111, $httpEngine->getPort());
    }

    /**
     * @covers HttpEngine::getSocksProxyUrl()
     * @covers HttpEngine::getSocksProxyUrl()
     */
    public function testAccessorSocksProxyUrl()
    {
        $httpEngine = new $this->classname('http://127.0.0.1/redirect301.php');
        $this->assertSame(null, $httpEngine->getSocksProxyUrl());

        $httpEngine->setSocksProxyUrl('80.234.12.124');
        $this->assertSame('80.234.12.124', $httpEngine->getSocksProxyUrl());

        $httpEngine->setSocksProxyUrl(true);
        $this->assertSame('', $httpEngine->getSocksProxyUrl());
    }

    /**
     * @covers HttpEngine::getSocksProxyPort()
     * @covers HttpEngine::getSocksProxyPort()
     */
    public function testAccessorSocksProxyPort()
    {
        $httpEngine = new $this->classname('http://127.0.0.1/redirect301.php');
        $this->assertSame(80, $httpEngine->getSocksProxyPort());

        $httpEngine->setSocksProxyPort(8080);
        $this->assertSame(8080, $httpEngine->getSocksProxyPort());
    }

}
