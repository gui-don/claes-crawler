<?php

namespace Claes\Test\Crawler;

use Claes\Core\Container;
use Claes\Crawler\HttpRequest;
use Claes\Crawler\HttpRequestHeader;
use Claes\Exception\DownloadException;

/**
 * Test class for HttpRequest
 */
class HttpRequestTest extends \PHPUnit_Framework_TestCase
{
    protected $CONTAINER_CLASS = '\Claes\Core\Container';
    protected $HTTP_REQUEST_CLASS = '\Claes\Crawler\HttpRequest';
    protected $HTTP_REQUEST_HEADER_CLASS = '\Claes\Crawler\HttpRequestHeader';
    protected $HTTP_ENGINE_CLASS = '\Claes\Crawler\CurlEngine';
    protected $HTTP_RESPONSE_CLASS = '\Claes\Crawler\HttpResponse';
    protected $HTTP_RESPONSE_HEADER_CLASS = '\Claes\Crawler\HttpResponseHeader';

    /**
     * @var \Claes\Core\Interfaces\ContainerInterface
     */
    protected $container;
    /**
     * @var \Claes\Crawler\Interfaces\HttpResponseInterface
     */
    protected $response;
    /**
     * @var \Claes\Crawler\Interfaces\HttpEngineInterface
     */
    protected $mockHttpEngine;

    protected $reponseCode = 200;
    protected $responseHeaders = "HTTP/1.1 200 OK\nHost: 127.0.0.1:5555\nContent-Language: fr\nContent-type: text/html; charset=UTF-8";
    protected $responseBody = 'content';
    protected $responseException = null;

    public function __construct()
    {
        $this->resetMocks();
    }

    protected function setMockResponseBody($content)
    {
        $this->responseBody = $content;
        $this->resetHttpEngine();
        $this->resetResponse();
        $this->resetMocks();
    }

    protected function setMockResponseCode($code)
    {
        $this->reponseCode = $code;
        $this->resetHttpEngine();
        $this->resetResponse();
        $this->resetMocks();
    }

    protected function setMockResponseHeaders($headers)
    {
        $this->responseHeaders = $headers;
        $this->resetHttpEngine();
        $this->resetResponse();
        $this->resetMocks();
    }

    protected function setMockException($exception)
    {
        $this->responseException = $exception;
        $this->resetHttpEngine();
        $this->resetMocks();
    }

    protected function getMockHttpEngine()
    {
        if (!empty($this->mockHttpEngine)) {
            return $this->mockHttpEngine;
        } else {
            return $this->resetHttpEngine();
        }
    }

    protected function getResponse()
    {
        if (!empty($this->response)) {
            return $this->response;
        } else {
            return $this->resetResponse();
        }
    }

    protected function resetResponse()
    {
        $responseHeader = new $this->HTTP_RESPONSE_HEADER_CLASS();
        $response = new $this->HTTP_RESPONSE_CLASS($this->responseBody, $this->reponseCode, $this->responseHeaders, $responseHeader);

        $this->response = $response;
        return $response;
    }

    protected function resetHttpEngine()
    {
        $mockHttpEngine = $this->getMockBuilder($this->HTTP_ENGINE_CLASS)->setMethods(['getResponseHeaders', 'getResponseCode', 'getResponseBody', 'get'])->setConstructorArgs(['http://127.0.0.1'])->getMock();
        $mockHttpEngine->method('getResponseHeaders')->willReturn($this->responseHeaders);
        $mockHttpEngine->method('getResponseCode')->willReturn($this->reponseCode);
        $mockHttpEngine->method('getResponseBody')->willReturn($this->responseBody);

        if (!$this->responseException) {
            $mockHttpEngine->method('get')->willReturn('');
        } else {
            $mockHttpEngine->method('get')->will($this->throwException(new $this->responseException()));
        }

        $this->mockHttpEngine = $mockHttpEngine;

        return $mockHttpEngine;
    }

    protected function resetMocks()
    {
        $mockContainer = $this->getMockBuilder($this->CONTAINER_CLASS)->setMethods(['getResponse', 'getRequest'])->disableOriginalConstructor()->getMock();

        $response = $this->getResponse();

        $requestHeader = new $this->HTTP_REQUEST_HEADER_CLASS();
        $mockHttpEngine = $this->getMockHttpEngine();
        $request = new $this->HTTP_REQUEST_CLASS($requestHeader, $mockHttpEngine, $mockContainer);

        $mockContainer->method('getResponse')->willReturn($response);
        $mockContainer->method('getRequest')->willReturn($request);

        $this->container = $mockContainer;
    }

    /**
     * @covers HttpRequest::__construct
     */
    public function testContructor()
    {
        $mock = $this->getMockBuilder($this->HTTP_REQUEST_CLASS)->disableOriginalConstructor()->getMock();
        $requestHeader = new $this->HTTP_REQUEST_HEADER_CLASS();
        $httpEngine = new $this->HTTP_ENGINE_CLASS('http://127.0.0.1');

        $mock->expects($this->once())->method('setHeaders')->with($this->equalTo($requestHeader))->willReturn($mock);

        $reflectedClass = new \ReflectionClass($this->HTTP_REQUEST_CLASS);
        $constructor = $reflectedClass->getConstructor();
        $constructor->invoke($mock, $requestHeader, $httpEngine, $this->container);
    }

    /**
     * @covers HttpRequest::changeUrl
     */
    public function testChangeUrl()
    {
        $url = 'https://google.com';

        $this->mockHttpEngine = $this->getMockBuilder($this->HTTP_ENGINE_CLASS)->setMethods(['setUrl'])->setConstructorArgs(['http://127.0.0.1'])->getMock();
        $this->mockHttpEngine->expects($this->once())->method('setUrl')->with($this->equalTo($url))->willReturn($this->mockHttpEngine);
        $this->resetMocks();

        $httpRequest = $this->container->getRequest('http://127.0.0.1');
        $httpRequest->changeUrl($url);
    }

    /**
     * @covers HttpRequest::changeUrl
     */
    public function testChangeUrlWithIp()
    {
        $url = 'http://127.0.0.1:5555';

        $this->mockHttpEngine = $this->getMockBuilder($this->HTTP_ENGINE_CLASS)->setMethods(['setUrl'])->setConstructorArgs(['https://google.com'])->getMock();
        $this->mockHttpEngine->expects($this->once())->method('setUrl')->with($this->equalTo($url))->willReturn($this->mockHttpEngine);
        $this->resetMocks();

        $httpRequest = $this->container->getRequest('http://127.0.0.1');
        $httpRequest->changeUrl($url);
    }

    /**
     * @covers HttpRequest::changeUrl
     */
    public function testChangeUrlFail()
    {
        $this->setExpectedExceptionRegExp('\Claes\Exception\RequestException', '#not valid#');

        $httpRequest = $this->container->getRequest('http://127.0.0.1');
        $httpRequest->changeUrl('not valid');
    }

    /**
     * @covers HttpRequest::send
     * @expectedException Claes\Exception\DownloadException
     */
    public function testSendCurlGetException()
    {
        $this->setMockException('Claes\Exception\CurlGetException');
        $httpRequest = $this->container->getRequest('http://127.0.0.1');
        $httpRequest->send();
    }

    /**
     * @covers HttpRequest::send
     * @expectedException Claes\Exception\DownloadException
     */
    public function testSendCurlUrlException()
    {
        $this->setMockException('Claes\Exception\CurlUrlException');
        $httpRequest = $this->container->getRequest('http://127.0.0.1');
        $httpRequest->send();
    }

    /**
     * @covers HttpRequest::send
     * @expectedException Claes\Exception\ResponseException
     */
    public function testSendCurlRandomException()
    {
        $this->setMockException('\Exception');
        $httpRequest = $this->container->getRequest('http://127.0.0.1');
        $httpRequest->send();
    }

    /**
     * @covers HttpRequest::send
     */
    public function testSend()
    {
        $this->mockHttpEngine = $this->getMockBuilder($this->HTTP_ENGINE_CLASS)->setMethods(['get', 'setHeaders'])->setConstructorArgs(['http://127.0.0.1'])->getMock();
        $this->mockHttpEngine->expects($this->once())->method('get');
        $this->mockHttpEngine->expects($this->once())->method('setHeaders')->with($this->equalTo($this->container->getRequest('http://127.0.0.1')->getHeaders()->convertCurl()));
        $this->resetMocks();

        $httpRequest = $this->container->getRequest('http://127.0.0.1');
        $httpRequest->send();
    }

    /**
     * @covers HttpRequest::send
     */
    public function testSendChangeStatus()
    {
        $this->resetMocks();

        $httpRequest = $this->container->getRequest('http://127.0.0.1');
        $firstStatus = $httpRequest->getStatus();
        $httpRequest->send();
        $this->assertNotEquals($firstStatus, $httpRequest->getStatus());
    }

    /**
     * @covers HttpRequest::send
     */
    public function testSendResponse()
    {
        $this->setMockException('');
        $this->setMockResponseCode(201);
        $this->setMockResponseHeaders("HTTP/1.1 201 Found\nHost: 127.0.0.1:5555\nContent-Language: fr\nContent-type: text/html; charset=UTF-8");

        $httpRequest = $this->container->getRequest('http://127.0.0.1');
        $response = $httpRequest->send();

        $this->assertTrue(is_a($response, $this->HTTP_RESPONSE_CLASS));
        $this->assertSame(201, $response->getCode());
        $this->assertSame($this->responseBody, $response->getContent());
        $this->assertSame('text/html', $response->getMime());
    }

    /**
     * @covers HttpRequest::send
     */
    public function testSendGzip()
    {
        $this->setMockResponseBody(gzencode('This is a compressed string in gzip'));
        $this->setMockResponseCode(200);
        $this->setMockResponseHeaders("HTTP/1.1 200 Found\nHost: 127.0.0.1:5555\nContent-Language: fr\nContent-type: text/plain; charset=UTF-8\nContent-encoding: gzip");

        $httpRequest = $this->container->getRequest('http://127.0.0.1');
        $response = $httpRequest->send();

        $this->assertSame(200, $response->getCode());
        $this->assertSame('This is a compressed string in gzip', $response->getContent());
        $this->assertSame('text/plain', $response->getMime());

        $this->setMockResponseHeaders("HTTP/1.1 200 Found\nHost: 127.0.0.1:5555\nContent-Language: fr\nContent-type: text/plain; charset=UTF-8");
        $httpRequest = $this->container->getRequest('http://127.0.0.1');
        $response = $httpRequest->send();

        $this->assertNotEquals('This is a compressed string in gzip', $response->getContent());
    }

    /**
     * @covers HttpRequest::send
     */
    public function testSendDeflate()
    {
        $this->setMockResponseBody(gzdeflate('This is a compressed string in deflate'));
        $this->setMockResponseCode(200);
        $this->setMockResponseHeaders("HTTP/1.1 200 Found\nHost: 127.0.0.1:5555\nContent-Language: fr\nContent-type: text/plain; charset=UTF-8\nContent-Encoding: deflate");

        $httpRequest = $this->container->getRequest('http://127.0.0.1');
        $response = $httpRequest->send();

        $this->assertSame(200, $response->getCode());
        $this->assertSame('This is a compressed string in deflate', $response->getContent());
        $this->assertSame('text/plain', $response->getMime());

        $this->setMockResponseHeaders("HTTP/1.1 200 Found\nHost: 127.0.0.1:5555\nContent-Language: fr\nContent-type: text/plain; charset=UTF-8");
        $httpRequest = $this->container->getRequest('http://127.0.0.1');
        $response = $httpRequest->send();

        $this->assertNotEquals('This is a compressed string in deflate', $response->getContent());
    }

    /**
     * @covers HttpRequest::getStatus()
     * @covers HttpRequest::setStatus()
     */
    public function testAccessorStatus()
    {
        $httpRequest = $this->container->getRequest('http://127.0.0.1');
        $this->assertSame(HttpRequest::STATUS_NOT_SEND, $httpRequest->getStatus());

        $httpRequest->setStatus(HttpRequest::STATUS_SEND);
        $this->assertSame(HttpRequest::STATUS_SEND, $httpRequest->getStatus());
    }

    /**
     * @covers HttpRequest::getHeaders()
     * @covers HttpRequest::setHeaders()
     */
    public function testAccessorHeaders()
    {
        $httpRequest = $this->container->getRequest('http://127.0.0.1');
        $this->assertTrue(is_subclass_of($httpRequest->getHeaders(), 'Claes\Crawler\Interfaces\HttpRequestHeaderInterface'));
        $firstId = spl_object_hash($httpRequest->getHeaders());

        $newObject = new HttpRequestHeader();
        $newId = spl_object_hash($newObject);
        $httpRequest->setHeaders($newObject);

        $this->assertTrue(is_subclass_of($httpRequest->getHeaders(), 'Claes\Crawler\Interfaces\HttpRequestHeaderInterface'));
        $this->assertNotEquals($firstId, spl_object_hash($httpRequest->getHeaders()));
        $this->assertSame($newId, spl_object_hash($httpRequest->getHeaders()));
    }

    /**
     * @covers HttpRequest::getHttpEngine()
     * @covers HttpRequest::setHttpEngine()
     */
    public function testAccessorHttpEngine()
    {
        $httpRequest = $this->container->getRequest('http://127.0.0.1');
        $this->assertTrue(is_subclass_of($httpRequest->getHttpEngine(), 'Claes\Crawler\Interfaces\HttpEngineInterface'));
        $firstId = spl_object_hash($httpRequest->getHttpEngine());

        $newObject = new \Claes\Crawler\CurlEngine('http://127.0.0.1');
        $newId = spl_object_hash($newObject);
        $httpRequest->setHttpEngine($newObject);

        $this->assertTrue(is_subclass_of($httpRequest->getHttpEngine(), 'Claes\Crawler\Interfaces\HttpEngineInterface'));
        $this->assertNotEquals($firstId, spl_object_hash($httpRequest->getHttpEngine()));
        $this->assertSame($newId, spl_object_hash($httpRequest->getHttpEngine()));
    }

    /**
     * @covers HttpRequest::getContainer()
     * @covers HttpRequest::setContainer()
     */
    public function testAccessorContainer()
    {
        $httpRequest = $this->container->getRequest('http://127.0.0.1');
        $this->assertTrue(is_subclass_of($httpRequest->getContainer(), 'Claes\Core\Interfaces\ContainerInterface'));
        $firstId = spl_object_hash($httpRequest->getContainer());

        $newObject = new Container();
        $newId = spl_object_hash($newObject);
        $httpRequest->setContainer($newObject);

        $this->assertTrue(is_subclass_of($httpRequest->getContainer(), 'Claes\Core\Interfaces\ContainerInterface'));
        $this->assertNotEquals($firstId, spl_object_hash($httpRequest->getContainer()));
        $this->assertSame($newId, spl_object_hash($httpRequest->getContainer()));
    }
}
