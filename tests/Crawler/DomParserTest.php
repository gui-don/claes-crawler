<?php

namespace Claes\Test\Crawler;

use \Claes\Crawler\DomParser;
use Rico\Lib\Chars;

/**
 * Test class for DomParser
 */
class DomParserTest extends \PHPUnit_Framework_TestCase
{
    protected $page1;
    protected $page2;

    protected function setUp()
    {
        $this->page1 = new DomParser(file_get_contents(__DIR__.'/files/page.html'));
        $this->page2 = new DomParser(file_get_contents(__DIR__.'/files/page2.html'));
    }

    public function providerGetFirstValueByTagName()
    {
        return [
            [12, false, false], // 0
            [new \Datetime(), false, false],
            [['not ok'], false, false],
            ['dontexist', null, false],
            ['h2', 'Pré-commandez le passe extensions pour The Witcher 3: Wild Hunt, recevez Witcher 1&2', false],
            ['i', '', false], // 5
            ['a', 'First a', false],
            ['*', 'PHP: DOMDocument::loadHTMLFile', '#(PHP\:\ [^:]+\:\:[^\ ]+)#i'],
        ];
    }

    public function providerGetFirstValueById()
    {
        return [
            [12, false, false], // 0
            [new \Datetime(), false, false],
            [['not ok'], false, false],
            ['dontexist', null, false],
            ['hidden_stuff', 'Are you “Ûtf-8” Àwäre?', false],
            ['big-spot-blur--1', '', false], // 5
            ['example-5335', 'Creating a Document', '#\#1\ (.*)#'],
        ];
    }

    public function providerGetFirstValueByClass()
    {
        return [
            [123, false, false], // 0
            [new \stdClass(), false, false],
            [['bad'], false, false],
            ['inexistent', null, false],
            ['top-nav-off-cart-count', '{{ cartCount }}', false],
            ['search', '', '#^$#'], // 5
            ['change-language', 'EnglishBrazilian PortugueseChinese (Simplified)FrenchGermanJapaneseKoreanRomanianRussianSpanishTurkishOther', '#Change language: (.*)#'],
        ];
    }

    public function providerGetFirstValueByCss()
    {
        return [
            [123, false], // 0
            [new \stdClass(), false],
            [['bad'], false],
            ['inexistent', null],
            ['.big-spot__price', 'gratuit€11.39'],
            ['.big-spot__price span', 'gratuit'], // 5
            ['.big-spot__price span:nth-child(2)', '€'],
            ['li:nth-child(2) a.datk-footer-link', 'Remerciements'],
            ['#hidden_stuff', 'Ûtf-8', '#“([^\“\”]+)”#'],
        ];
    }

    public function providerSlimDomParser()
    {
        return [
            [123, false], // 0
            [new \stdClass(), false],
            [['bad'], false],
            ['inexistent', []],
            ['#footer', ['Copyright © 2001-2015 The PHP Group My PHP.net Contact Other PHP.net sites Mirror sites Privacy policy']],
            ['#V51738', ['-9']], // 5
            ['#empty', ['']],
            ['.tbody', ['5.4.0 Added options parameter.']],
            ['.title', ['Descrição', 'Parâmetros', 'Valor Retornado', 'Erros', 'Exemplos', 'Changelog', 'Veja Também', 'User Contributed Notes 6 notes']],
            ['.empty', ['']],
            ['.para li:nth-child(3) a', ['DOMDocument::saveHTMLFile()']], // 10
            ['.footmenu a', ['Copyright © 2001-2015 The PHP Group', 'My PHP.net', 'Contact', 'Other PHP.net sites', 'Mirror sites', 'Privacy policy']],
        ];
    }


    public function providerGetAllUrlInsideCss()
    {
        return [
            [123, false], // 0
            [new \stdClass(), false],
            [['bad'], false],
            ['inexistent', []],
            ['#51738', ['/manual/vote-note.php?id=51738&page=domdocument.loadhtmlfile&vote=up', '/manual/vote-note.php?id=51738&page=domdocument.loadhtmlfile&vote=down', '#51738', '#51738']],
            ['#singleimage', ['anotherimage.jpg']], // 5
            ['#secondimage', ['image.jpg']],
            ['#usernotes .action img', ['/images/notes-add@2x.png']],
            ['.edit-bug', ['https://edit.php.net/?project=PHP&perm=pt_BR/domdocument.loadhtmlfile.php', 'https://bugs.php.net/report.php?bug_type=Documentation+problem&manpage=domdocument.loadhtmlfile']],
            ['p.para .link', ['libxml.constants.php', 'function.libxml-use-internal-errors.php', 'function.libxml-use-internal-errors.php']],
            ['img.img-ok', ['anotherimage.jpg']], // 10
            ['div.head span', ['/manual/add-note.php?sect=domdocument.loadhtmlfile&redirect=https://php.net/manual/pt_BR/domdocument.loadhtmlfile.php', '/images/notes-add@2x.png']],
        ];
    }

    /**
     * @covers DomParser::__construct()
     */
    public function testConstruct()
    {
        $this->assertInstanceOf('\DomDocument', $this->page1);
        $this->assertInstanceOf('\DomDocument', $this->page2);
    }

    /**
	 * @covers DomParser::getFirstValueByTagName()
     * @dataProvider providerGetFirstValueByTagName
	 */
	public function testGetFirstValueByTagName($tagName, $expected, $regexp)
    {
        if (empty($regexp)) {
            $this->assertSame($expected, $this->page1->getFirstValueByTagName($tagName));
        } else {
            $this->assertSame($expected, $this->page2->getFirstValueByTagName($tagName, $regexp));
        }
	}

    /**
	 * @covers DomParser::getFirstValueById()
     * @dataProvider providerGetFirstValueById
	 */
	public function testGetFirstValueById($id, $expected, $regexp)
    {
        if (empty($regexp)) {
            $this->assertSame($expected, $this->page1->getFirstValueById($id));
        } else {
            $this->assertSame($expected, $this->page2->getFirstValueById($id, $regexp));
        }
	}

    /**
	 * @covers DomParser::getFirstValueByClass()
     * @dataProvider providerGetFirstValueByClass
	 */
	public function testGetFirstValueByClass($class, $expected, $regexp)
    {
        if (empty($regexp)) {
            $this->assertSame($expected, $this->page1->getFirstValueByClass($class));
        } else {
            $this->assertSame($expected, $this->page2->getFirstValueByClass($class, $regexp));
        }
	}

    /**
	 * @covers DomParser::getFirstValueByCss()
     * @dataProvider providerGetFirstValueByCss
	 */
	public function testGetFirstValueCss($class, $expected, $regexp = '')
    {
        if (empty($regexp)) {
            $this->assertSame($expected, $this->page1->getFirstValueByCss($class));
        } else {
            $this->assertSame($expected, $this->page1->getFirstValueByCss($class, $regexp));
        }
	}

    /**
	 * @covers DomParser::slimNodeParser()
     * @dataProvider providerSlimDomParser
	 */
	public function testSlimNodeParser($css, $expected)
    {
        $sharpenedDomParsers = $this->page2->slimDomParser($css);

        if ($expected || $expected === '') {
            for ($i = 0, $j = count($sharpenedDomParsers); $i < $j; $i++) {
                $this->assertInstanceOf('\Claes\Crawler\DomParser', $sharpenedDomParsers[$i]);
                $this->assertSame(trim($expected[$i]), Chars::normalize(trim($sharpenedDomParsers[$i]->textContent)));
            }
        } else {
            $this->assertSame($expected, $sharpenedDomParsers);
        }
	}

    /**
     * @covers DomParser::getDomXPath()
     */
    public function testGetDomXPath()
    {
        $this->assertInstanceOf('\DomXPath', $this->page1->getDomXPath());
        $this->assertInstanceOf('\DomXPath', $this->page2->getDomXPath());
    }

    /**
     * @covers DomParser::getAllUrlInsideCss()
     * @dataProvider providerGetAllUrlInsideCss
     */
    public function testGetAllUrlInsideCss($value, $expected)
    {
        $this->assertEquals($expected, $this->page2->getAllUrlInsideCss($value));
    }
}
