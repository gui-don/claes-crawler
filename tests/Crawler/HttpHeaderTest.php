<?php

namespace Claes\Test\Crawler;

use Claes\Crawler\HttpHeader;

class HttpHeaderTest extends \PHPUnit_Framework_TestCase
{
    public function providerAddHeader()
    {
        return [
            [0, 'test', false],
            [new \DateTime('now'), 'test', false],
            ['ok', 0.73, false],
            ['ok', 'test', true],
            ['X-Firefox-Spdy', '3.1', true],
        ];
    }

    /**
     * @covers HttpHeader::addHeader()
     * @dataProvider providerAddHeader
     */
    public function testAddHeader($header, $value, $expected)
    {
        $httpResponseHeader = new HttpHeader();

        if ($expected) {
            $httpResponseHeader->addHeader($header, $value);
            $this->assertArrayHasKey(strtolower($header), $httpResponseHeader->getHeaders());
            $this->assertSame($header, $httpResponseHeader->getHeaders()[strtolower($header)][0]);
            $this->assertSame($value, $httpResponseHeader->getHeaders()[strtolower($header)][1]);
        } else {
            $this->assertFalse($httpResponseHeader->addHeader($header, $value));
        }
    }

    /**
     * @covers HttpHeader::__toString()
     */
    public function testToString()
    {
        $httpHeader = new HttpHeader();

        $httpHeader->addHeader('Host', 'www.gog.com');
        $httpHeader->addHeader('Connection', 'keep-alive');
        $httpHeader->addHeader('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8');
        $httpHeader->addHeader('Cache-Control', 'max-age=0');
        $httpHeader->addHeader('User-Agent', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36');
        $httpHeader->addHeader('DNT', '1');
        $this->assertSame('Host: www.gog.com
Connection: keep-alive
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Cache-Control: max-age=0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36
DNT: 1
', $httpHeader->__toString());
    }
}
