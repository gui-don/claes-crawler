<?php

namespace Claes\Test\Crawler;

use Claes\Crawler\HttpResponse;
use Claes\Crawler\HttpResponseHeader;

/**
 * Test class for HttpResponse
 */
class HttpResponseTest extends \PHPUnit_Framework_TestCase
{
    const TEST_DIR = __DIR__ . 'tests/';
    public static $strFile = 'file.txt';


    public static function setUpBeforeClass()
    {
        mkdir(self::TEST_DIR);
        file_put_contents(self::TEST_DIR.self::$strFile, str_repeat(mt_rand(0,9), 5000));
    }

    public static function tearDownAfterClass()
    {
        unlink(self::TEST_DIR.self::$strFile);
        rmdir(self::TEST_DIR);
    }

    /**
     * @covers HttpResponse::__construct()
     */
    public function testContructorSuccess()
    {
        $rawHeader = "HTTP/1.1 200 OK
Host: 127.0.0.1:5555
Content-Language: fr
Content-type: text/html; charset=UTF-8";
        $convertHeaders = ['Host' => '127.0.0.1:5555', 'Content-Language' => 'fr', 'Content-type' => 'text/html; charset=UTF-8'];
        $headers = [[0 => 'Host', 1 => '127.0.0.1:5555'], [0 => 'Content-Language', 1 => 'fr'], [0 => 'Content-type', 1 => 'text/html; charset=UTF-8']];

        $mock = $this->getMockBuilder('Claes\Crawler\HttpResponse')->disableOriginalConstructor()->getMock();
        $mockResponseHeader = $this->getMockBuilder('Claes\Crawler\HttpResponseHeader')->setMethods(['convert', 'getHeaders'])->getMock();
        $mockResponseHeader->method('convert')->will($this->returnArgument($convertHeaders));
        $mockResponseHeader->method('getHeaders')->will($this->returnArgument($headers));

        $mock->expects($this->once())->method('setContent')->with($this->equalTo('Some content'))->willReturn($mock);
        $mock->expects($this->once())->method('setCode')->with($this->equalTo(200))->willReturn($mock);
        $mock->expects($this->once())->method('setHeaders')->with($this->equalTo($mockResponseHeader))->willReturn($mock);
        $mock->expects($this->once())->method('setMime')->with($this->equalTo('text/html; charset=UTF-8'))->willReturn($mock);

        $reflectedClass = new \ReflectionClass('Claes\Crawler\HttpResponse');
        $constructor = $reflectedClass->getConstructor();
        $constructor->invoke($mock, 'Some content', 200, $rawHeader, $mockResponseHeader);
    }

    /**
     * @covers HttpResponse::save
     */
    public function testSave()
    {
        $savePath = 'Test/files/Très~SpécialÿChar .txt';

        $httpResponse = new HttpResponse('Some content', 200, "Status: 200 OK\nContent-type: text/html; charset=UTF-8", new HttpResponseHeader());

        $this->assertTrue($httpResponse->save($savePath));
        $this->assertSame('Some content', file_get_contents($savePath));

        unlink($savePath);
    }

    /**
     * @covers HttpResponse::save
     */
    public function testSaveWithoutFilename()
    {
        $savePath = 'Test/files/';
    
        $httpResponse = new HttpResponse(file_get_contents(__DIR__.'/files/claes.png'), 200, "Status: 200 OK\nContent-type: image/png", new HttpResponseHeader());

        $this->assertTrue($httpResponse->save($savePath));
        $this->assertSame(file_get_contents(__DIR__.'/files/claes.png'), file_get_contents($savePath.'2ea77b22d4137e573aa24a7cc580bd84.png'));

        unlink($savePath.'2ea77b22d4137e573aa24a7cc580bd84.png');
    }

    /**
     * @covers HttpResponse::save
     */
    public function testSaveFail()
    {
        $httpResponse = new HttpResponse('Some content', 200, "Status: 200 OK\nContent-type: text/plain", new HttpResponseHeader());

        // Not a string tests
        $this->assertFalse($httpResponse->save(450));
        $this->assertFalse($httpResponse->save(['notgood']));
        $this->assertFalse($httpResponse->save(new \stdClass()));
        $this->assertFalse($httpResponse->save(true));
    }

    /**
     * @covers HttpResponse::save
     */
    public function testSaveWithNewPath()
    {
        $savePath = 'files/new/path/test.txt';

        $httpResponse = new HttpResponse('New content', 200, "Status: 200 OK\nContent-type: text/plain", new HttpResponseHeader());

        $this->assertTrue($httpResponse->save($savePath));
        $this->assertSame('New content', file_get_contents($savePath));

        unlink($savePath);
        rmdir('files/new/path/');
        rmdir('files/new/');
    }

    /**
     * @covers HttpResponse::save
     */
    public function testSaveWithOverride()
    {
        $savePath = 'files/over.txt';

        $httpResponse = new HttpResponse('This is the 1st content.', 200, "Status: 200 OK\nContent-type: text/plain", new HttpResponseHeader());

        $this->assertTrue($httpResponse->save($savePath));
        $this->assertSame('This is the 1st content.', file_get_contents($savePath));

        $httpResponse->setContent('SECOND CONTENT');

        $this->assertTrue($httpResponse->save($savePath));
        $this->assertSame('SECOND CONTENT', file_get_contents($savePath));

        unlink($savePath);
    }

    /**
     * @covers HttpResponse::save
     * @expectedException Claes\Exception\FileException
     */
    public function testSaveWithoutOverride()
    {
        $httpResponse = new HttpResponse('Override content', 200, "Status: 200 OK\nContent-type: text/plain", new HttpResponseHeader());

        $httpResponse->save(self::TEST_DIR.self::$strFile, false);
    }

    /**
     * @covers HttpResponse::getCode()
     * @covers HttpResponse::setCode()
     */
    public function testAccessorCode()
    {
        $httpResponse = new HttpResponse('This is the 1st content.', 200, '', new HttpResponseHeader());
        $this->assertSame(200, $httpResponse->getCode());

        $httpResponse->setCode(500);
        $this->assertSame(500, $httpResponse->getCode());
    }

    /**
     * @covers HttpResponse::getContent()
     * @covers HttpResponse::setContent()
     */
    public function testAccessorContent()
    {
        $httpResponse = new HttpResponse('This is the 1st content.', 200, '', new HttpResponseHeader());
        $this->assertSame('This is the 1st content.', $httpResponse->getContent());

        $httpResponse->setContent('Another content');
        $this->assertSame('Another content', $httpResponse->getContent());
    }

    /**
     * @covers HttpResponse::getMime()
     * @covers HttpResponse::setMime()
     */
    public function testAccessorMime()
    {
        $httpResponse = new HttpResponse('This is the 1st content.', 200, "Content-type: application/json; charset=UTF-8", new HttpResponseHeader());
        $this->assertSame('application/json', $httpResponse->getMime());

        $httpResponse->setMime('plain/text');
        $this->assertSame('plain/text', $httpResponse->getMime());
    }

    /**
     * @covers HttpResponse::getHeaders()
     * @covers HttpResponse::setHeaders()
     */
    public function testAccessorHeaders()
    {
        $httpResponse = new HttpResponse('This is the 1st content.', 200, "Content-type: application/json; charset=UTF-8", new HttpResponseHeader());
        $this->assertTrue(is_subclass_of($httpResponse->getHeaders(), 'Claes\Crawler\Interfaces\HttpResponseHeaderInterface'));

        $httpResponseHeaders = new HttpResponseHeader();
        $firstId = spl_object_hash($httpResponseHeaders);
        $httpResponse->setHeaders($httpResponseHeaders);
        $this->assertTrue(is_subclass_of($httpResponse->getHeaders(), 'Claes\Crawler\Interfaces\HttpResponseHeaderInterface'));

        $this->assertSame($firstId, spl_object_hash($httpResponse->getHeaders()));

    }
}
