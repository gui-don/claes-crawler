<?php

namespace Claes\Exception;

use Claes\Exception\CurlException;

class CurlUrlException extends CurlException
{
}
