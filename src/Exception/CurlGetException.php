<?php

namespace Claes\Exception;

use Claes\Exception\CurlException;

class CurlGetException extends CurlException
{
}
