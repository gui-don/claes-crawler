<?php

namespace Claes\Client;

use Claes\Core\Interfaces\ContainerInterface;
use Claes\Crawler\Interfaces\HttpRequestHeaderInterface;
use Claes\Crawler\Interfaces\HttpEngineInterface;
use Claes\Crawler\Interfaces\HttpRequestInterface;
use Claes\Crawler\HttpRequest;

/**
 * Handles HTTP requests like a Firefox browser
 */
class FirefoxRequestWrapper extends HttpRequest implements HttpRequestInterface
{
    /**
     * {@inheritdoc}
     */
    public function __construct(HttpRequestHeaderInterface $requestHeaders, HttpEngineInterface $httpEngine, ContainerInterface $container)
    {
        parent::__construct($requestHeaders, $httpEngine, $container);

        $this->setDefaultHeaders();
    }

    /**
     * {@inheritdoc}
     */
    public function send()
    {
        $this->organizeHeaders();

        return parent::send();
    }

    /**
     * Organize headers
     */
    public function organizeHeaders()
    {
        $this->headers->setHost($this->headers->getHost());
        $this->headers->setUserAgent($this->headers->getUserAgent());
        $this->headers->setAccept($this->headers->getAccept());
        $this->headers->setAcceptLanguage($this->headers->getAcceptLanguage());
        $this->headers->setAcceptEncoding($this->headers->getAcceptEncoding());
        $this->headers->setDnt($this->headers->getDnt());
        $this->headers->setCookie($this->headers->getCookie());
        $this->headers->setConnection($this->headers->getConnection());
        $this->headers->setPragma($this->headers->getPragma());
        $this->headers->setIfModifiedSince($this->headers->getIfModifiedSince());
        $this->headers->setIfNoneMatch($this->headers->getIfNoneMatch());
        $this->headers->setCacheControl($this->headers->getCacheControl());
    }

    /**
     * Set default headers for this browser
     */
    private function setDefaultHeaders()
    {
        $this->headers->setAcceptCharset('');
        $this->headers->setAcceptLanguage('');
        $this->headers->setCacheControl('');
        $this->headers->setHost(parse_url($this->getHttpEngine()->getUrl(), PHP_URL_HOST));
        $this->headers->setUserAgent('Mozilla/5.0 (Windows NT 6.1; rv:44.0) Gecko/20100101 Firefox/44.0');
        $this->headers->setAccept('text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8');
        $this->headers->setAcceptEncoding('gzip, deflate');
        $this->headers->setDnt(1);
        $this->headers->setConnection('keep-alive');
    }
}
