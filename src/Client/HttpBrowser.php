<?php

namespace Claes\Client;

use Claes\Core\Interfaces\ContainerInterface;
use Claes\Crawler\Interfaces\HttpRequestInterface;
use Claes\Crawler\Interfaces\HttpResponseInterface;
use Claes\Crawler\Interfaces\DomParserInterface;
use Claes\Client\Interfaces\HttpBrowserInterface;
use Claes\Exception\ResponseException;
use Rico\Lib\Checker;

/**
 * HTTP browser - Permits to navigate through HTTP URLs
 */
class HttpBrowser implements HttpBrowserInterface
{
    /**
     * HTTP request object
     * @var HttpRequestInterface
     */
    protected $request;
    /**
     * HTTP response object
     * @var HttpResponseInterface
     */
    protected $response;
    /**
     * DOM parser object
     * @var DomParserInterface
     */
    protected $parser;
    /**
     * Services container
     * @var ContainerInterface
     */
    protected $container;
    /**
     * Delay in seconds between two requests to make the crawler more human-like
     * @var int
     */
    protected $delay = 0;
    /**
     * Delay variability in seconds to be even more human-like
     * @var int Dem
     */
    protected $delayMargin = 0;
    /**
     * Determine whether or not the referer to be set and send at each request
     * @var bool
     */
    protected $disableReferer = false;
    /**
     * Determine whether or not an error should rise if the HTTP code is not 200
     * @var bool
     */
    protected $strictMode = true;
    /**
     * Determine whether the current request is the first one or not
     * @var bool
     */
    private $firstTime = true;

    /**
     * Create a new HTTP browser
     */
    public function __construct()
    {
        $container = new \Claes\Core\Container();
        $container->registerRequest('\Claes\Crawler\HttpRequest', '\Claes\Crawler\HttpRequestHeader', '\Claes\Crawler\CurlEngine');
        $container->registerResponse('\Claes\Crawler\HttpResponse', '\Claes\Crawler\HttpResponseHeader');
        $container->registerDomParser('\Claes\Crawler\DomParser');

        $this->setContainer($container);
    }

    /**
     * Wait for the delay
     */
    private function waitForDelay()
    {
        $minSleep = ($this->getDelay() * 1000000) - mt_rand(0, $this->getDelayMargin() * 1000000);
        $maxSleep = ($this->getDelay() * 1000000) + mt_rand(0, $this->getDelayMargin() * 1000000);
        $sleep = mt_rand($minSleep, $maxSleep);

        if (!$this->getFirstTime() && $sleep >= 0) {
            usleep($sleep);
        } else {
            $this->setFirstTime(false);
        }
    }

    /**
     * {@inheritDoc}
     */
    private function load($url)
    {
        $this->waitForDelay();

        if ($this->getDisableReferer()) {
            $this->getRequest()->getHeaders()->setReferer('');
        }

        $this->setResponse($this->getRequest()->send());

        if ($this->getStrictMode() && $this->getResponse()->getCode() != 200) {
            throw new ResponseException('Requesting “'.$url.'” returns an error (CODE: '.$this->getResponse()->getCode().')');
        }

        $this->setParser($this->container->getDomParser($this->getResponse()->getContent()));

        if ($this->getResponse()->getHeaders()->getSetCookie()) {
            $newCookies = [];
            parse_str(preg_replace('#\; ?#', '&', trim($this->getResponse()->getHeaders()->getSetCookie())), $newCookies);
            $oldCookies = [];
            parse_str(preg_replace('#\; ?#', '&', $this->getRequest()->getHeaders()->getCookie()), $oldCookies);

            $this->getRequest()->getHeaders()->setCookie(http_build_query($newCookies + $oldCookies, '', '; '));
        }

        if (!$this->getDisableReferer()) {
            $this->getRequest()->getHeaders()->setReferer($url);
        }
    }

    /**
     * Simulate click on a link (download a new page to parse)
     * @param string $url New link to download
     */
    public function get($url)
    {
        if (null !== $this->getRequest()) {
            $this->getRequest()->changeUrl($url);
        } else {
            $this->setRequest($this->getContainer()->getRequest($url));
        }

        if (parse_url($url, PHP_URL_HOST) == $this->getRequest()->getHeaders()->getHost()) {
            $this->setFirstTime(false);
        } else {
            $this->setFirstTime(true);
        }

        if (!Checker::isIp(parse_url($url, PHP_URL_HOST))) {
            $this->getRequest()->getHeaders()->setHost(parse_url($url, PHP_URL_HOST));
        }

        $this->load($url);
    }

    /**
     * @return HttpRequestInterface
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @return HttpResponseInterface
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return DomParserInterface
     */
    public function getParser()
    {
        return $this->parser;
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @return int
     */
    public function getDelay()
    {
        return $this->delay;
    }

    /**
     * @return int
     */
    public function getDelayMargin()
    {
        return $this->delayMargin;
    }

    /**
     * @return bool
     */
    public function getDisableReferer()
    {
        return $this->disableReferer;
    }

    /**
     * @return bool
     */
    public function getStrictMode()
    {
        return $this->strictMode;
    }

    /**
     * @return bool
     */
    public function getFirstTime()
    {
        return $this->firstTime;
    }

    /**
     * @param HttpRequestInterface $request
     * @return self
     */
    public function setRequest(HttpRequestInterface $request)
    {
        $this->request = $request;
        return $this;
    }

    /**
     * @param HttpResponseInterface $response
     * @return self
     */
    public function setResponse(HttpResponseInterface $response)
    {
        $this->response = $response;
        return $this;
    }

    /**
     * @param DomParserInterface $parser
     * @return self
     */
    public function setParser(DomParserInterface $parser)
    {
        $this->parser = $parser;
        return $this;
    }

    /**
     * @param ContainerInterface $container
     * @return self
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
        return $this;
    }

    /**
     * @param int $delay
     * @return self
     */
    public function setDelay($delay)
    {
        $this->delay = $delay;
        return $this;
    }

    /**
     * @param int $delayMargin
     * @return self
     */
    public function setDelayMargin($delayMargin)
    {
        $this->delayMargin = $delayMargin;
        return $this;
    }

    /**
     * @param bool $disableReferer
     * @return self
     */
    public function setDisableReferer($disableReferer)
    {
        $this->disableReferer = $disableReferer;
        return $this;
    }

    /**
     * @param bool $strictMode
     * @return self
     */
    public function setStrictMode($strictMode)
    {
        $this->strictMode = $strictMode;
        return $this;
    }

    /**
     * @param bool $firstTime
     * @return self
     */
    public function setFirstTime($firstTime)
    {
        $this->firstTime = $firstTime;
        return $this;
    }

}
