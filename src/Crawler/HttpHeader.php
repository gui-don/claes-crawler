<?php

namespace Claes\Crawler;

class HttpHeader
{
    protected $headers = [];

    /**
     * {@inheritdoc}
     */
    public function __construct($rawHeaders = '')
    {
        $this->hydrate($rawHeaders);
    }

    public function __toString()
    {
        $headers = '';
        foreach ($this->convert() as $header => $value) {
            $headers .= $header.': '.$value.PHP_EOL;
        }

        return $headers;
    }

    /**
     * {@inheritdoc}
     */
    public function hydrate($rawHeaders)
    {
        preg_match_all('#^([A-Za-z0-9\-]+)\s?\:\s?([^\r\n\f\x0b\x85]+)#m', $rawHeaders, $matches);

        for ($i = 0, $j = count($matches[0]); $i < $j; $i++) {
            if (isset($this->headers[strtolower($matches[1][$i])][0])) {
                // Handle cookie exception - append if there are tow setCookie headers
                if (strtolower($matches[1][$i]) != 'set-cookie') {
                    $header = $this->headers[strtolower($matches[1][$i])];
                    $header[1] = $matches[2][$i];
                    unset($this->headers[strtolower($matches[1][$i])]);
                    $this->headers[strtolower($matches[1][$i])] = $header;
                } else {
                    $header = $this->headers[strtolower($matches[1][$i])];
                    $header[1] .= empty($this->headers[strtolower($matches[1][$i])][1]) ? $matches[2][$i] : '; '.$matches[2][$i];
                    unset($this->headers[strtolower($matches[1][$i])]);
                    $this->headers[strtolower($matches[1][$i])] = $header;
                }
            } else {
                $this->addHeader($matches[1][$i], $matches[2][$i]);
            }
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function convert()
    {
        $headers = [];
        foreach ($this->headers as $header) {
            if (!empty($header[1])) {
                $headers[$header[0]] = $header[1];
            }
        }

        return $headers;
    }

    /**
     * {@inheritdoc}
     */
    public function convertCurl()
    {
        $headers = [];
        foreach ($this->headers as $header) {
            if (!empty($header[1])) {
                $headers[] = $header[0].': '.$header[1];
            }
        }

        return $headers;
    }

    /**
     * {@inheritdoc}
     */
    public function addHeader($header, $value)
    {
        if (!is_string($header) || !is_string($value)) {
            return false;
        }

        $this->headers[strtolower($header)][0] = $header;
        $this->headers[strtolower($header)][1] = $value;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * {@inheritdoc}
     */
    public function getRaw()
    {
        $headers = '';
        foreach ($this->headers as $header) {
            if (!empty($header[1])) {
                $headers .= $header[0].': '.$header[1]."\n";
            }
        }
        $headers .= "\n";

        return $headers;
    }
}
