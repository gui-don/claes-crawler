<?php

namespace Claes\Crawler\Interfaces;

/**
 * HttpHeader Interface
 */
interface HttpHeaderInterface
{
    /**
     * Create a new header object
     * @param $rawHeaders Raw header string
     */
    public function __construct($rawHeaders = '');

    /**
     * Convert an Http Header object into an array
     * @return string[]
     */
    public function convert();

    /**
     * Convert a HttpHeader objet into an array compatible with CuRL header format
     * @return string[]
     */
    public function convertCurl();

    /**
     * Hydrate raw headers into a readable HttpHeader object
     * @param $rawHeaders Raw Header string
     * @return HttpHeaderInterface
     */
    public function hydrate($rawHeader);

    /**
     * Add another header
     * @param string $header Header
     * @param string $value Value of the header
     * @return HttpHeaderInterface
     */
    public function addHeader($header, $value);

    /**
     * Get headers string
     * @return string[]
     */
    public function getHeaders();

    /**
     * Get raw headers
     * @return string
     */
    public function getRaw();
}
