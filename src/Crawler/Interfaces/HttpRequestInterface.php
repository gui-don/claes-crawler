<?php

namespace Claes\Crawler\Interfaces;

use Claes\Crawler\Interfaces\HttpRequestHeaderInterface;
use Claes\Crawler\Interfaces\HttpEngineInterface;
use Claes\Core\Interfaces\ContainerInterface;

/**
 * HttpRequestInterface
 */
interface HttpRequestInterface
{
    /**
     * Create a new HTTP request
     * @param HttpRequestHeaderInterface $requestHeader
     * @param HttpEngineInterface $httpEngine
     * @param ContainerInterface $container
     */
    public function __construct(HttpRequestHeaderInterface $requestHeader, HttpEngineInterface $httpEngine, ContainerInterface $container);

    /**
     * Send a http request and get the response
     * @return HttpResponseInterface
     * @throws DownloadException
     */
    public function send();

    /**
     * Change URL
     * @param string $url
     */
    public function changeUrl($url);

    /**
     * @return int
     */
    public function getStatus();

    /**
     * @return HttpRequestHeaderInterface
     */
    public function getHeaders();

    /**
     * @return HttpEngineInterface
     */
    public function getHttpEngine();

    /**
     * @return ContainerInterface
     */
    public function getContainer();

    /**
     * @param int $status
     * @return self
     */
    public function setStatus($status);

    /**
     * @param HttpRequestHeaderInterface $headers
     * @return self
     */
    public function setHeaders(HttpRequestHeaderInterface $headers);

    /**
     * @param HttpEngineInterface $httpEngine
     * @return self
     */
    public function setHttpEngine(HttpEngineInterface $httpEngine);

    /**
     * @param ContainerInterface $container
     * @return self
     */
    public function setContainer(ContainerInterface $container);
}
