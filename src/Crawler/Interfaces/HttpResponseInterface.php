<?php

namespace Claes\Crawler\Interfaces;

use Claes\Crawler\Interfaces\HttpResponseHeaderInterface;

/**
 * HttpResponse interface
 */
interface HttpResponseInterface
{
    /**
     * Create a new HttpResponse object
     * @param string $content Content of the response
     * @param int $code Code of the response
     * @param string $rawHeaders Raw HTTP headers
     * @param HttpResponseHeaderInterface $httpResponseHeader
     */
    public function __construct($content, $code, $rawHeaders, HttpResponseHeaderInterface $httpResponseHeader);

    /**
     * Save the response in a file
     * @param string $path
     * @param type $override
     */
    public function save($path, $override = true);

    /**
     * Get response content
     * @return string
     */
    public function getContent();

    /**
     * Get response MIME
     * @return string
     */
    public function getMime();

    /**
     * Get HTTP response code
     * @return int
     */
    public function getCode();

    /**
     * Get HTTP response header object
     * @return HttpResponseHeaderInterface
     */
    public function getHeaders();

    /**
     * Set response content
     * @param string $content
     * @return self
     */
    public function setContent($content);

    /**
     * Set response MIME
     * @param string $mime
     * @return self
     */
    public function setMime($mime);

    /**
     * Set HTTP response code
     * @param int $code
     * @return self
     */
    public function setCode($code);

    /**
     * Set HTTP response header object
     * @param HttpResponseHeaderInterface $headers
     * @return self
     */
    public function setHeaders(HttpResponseHeaderInterface $headers);
}