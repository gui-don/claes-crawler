<?php

namespace Claes\Crawler\Interfaces;

interface DomParserInterface
{
    /**
     * Create a new DOMParser
     * @param string $rawBody Raw DOM
     */
    public function __construct($rawBody);

    /**
     * Initialize the DomParser with $rawBody
     * @param string $rawBody
     */
    public function setContent($rawBody);

    /**
     * Get the content a node value by tag name
     * @param string $tagName Node
     * @param string $regExtract Regular expression to extract what is needed (use a capturing brackets)
     * @return string Node value
     */
    public function getFirstValueByTagName($tagName, $regExtract = null);

    /**
     * Get the content of a node value by id
     * @param string $id DOM Id
     * @param string $regExtract Regular expression to extract what is needed (use a capturing brackets)
     * @return string Node value or null if nothing is found or false if an error occured
     */
    public function getFirstValueById($id, $regExtract = null);

    /**
     * Get the content of a node by class
     * @param string $class DOM class
     * @param string $regExtract Regular expression to extract what is needed (use a capturing brackets)
     * @return string Node value or null if nothing is found or false if an error occured
     */
    public function getFirstValueByClass($class, $regExtract = null);

    /**
     * Get the content of a node value by css
     * @param string $css DOM expression
     * @param string $regExtract Regular expression to extract what is needed (use a capturing brackets)
     * @return string Node value or null if nothing is found or false if an error occured
     */
    public function getFirstValueByCss($css, $regExtract = null);

    /**
     * Get all the URLs (images and links) inside all the blocks defined by a CSS selector
     * @param string $css DOM CSS selector
     * @return string[] Urls
     */
    public function getAllUrlInsideCss($css);

    /**
     * Slim the parent DomParser into another sharpened DomParser with the root node corresponding to the CSS selector
     * @param string $css Dom CSS selector
     * @return \self[] or false if an error occured
     */
    public function slimDomParser($css);

    /**
     * @return \DOMXPath
     */
    public function getDomXPath();
}
