<?php

namespace Claes\Crawler\Interfaces;

interface HttpEngineInterface
{
    /**
     * Create a new http engine
     * @param string $url
     */
    public function __construct($url);

    /**
     * Destruct the http engine
     */
    public function __destruct();
    
    /**
     * Do a GET request
     */
    public function get();

    /**
     * Get the information of the last transaction - organised in an array with curl getinfo constant keys
     * @see https://secure.php.net/manual/en/function.curl-getinfo.php
     * @return string[]
     */
    public function getTransactionInformation();

    /**
     * Get raw response headers
     * @return string Raw response headers
     */
    public function getResponseHeaders();

    /**
     * Get response code
     * @return int Response code
     */
    public function getResponseCode();

    /**
     * Get raw response body
     * @return string Raw response body
     */
    public function getResponseBody();

    /**
     * Get url to handle
     * @return string
     */
    public function getUrl();

    /**
     * Get HTTP version to use
     * @return float
     */
    public function getHttpVersion();

    /**
     * Get SSL version to use
     * @return float
     */
    public function getSslVersion();

    /**
     * Get if SSL certificate must be checked or not
     * @return bool
     */
    public function getSslVerify();

    /**
     * Get whether or not to follow redirections
     * @return bool
     */
    public function getFollowRedirections();

    /**
     * Get the port to use
     * @return int
     */
    public function getPort();

    /**
     * Get the headers to be sent
     * @return string[]
     */
    public function getHeaders();

    /**
     * Get socks proxy URL
     * @return string
     */
    public function getSocksProxyUrl();

    /**
     * Get socks proxy port
     * @return int
     */
    public function getSocksProxyPort();

    /**
     * Set url to handle
     * @param string $url
     * @return self
     */
    public function setUrl($url);

    /**
     * Set HTTP version to use
     * @param float $httpVersion
     * @return self
     */
    public function setHttpVersion($httpVersion);

    /**
     * Set SSL version to use
     * @param float $sslVersion
     * @return self
     */
    public function setSslVersion($sslVersion);

    /**
     * Set if SSL certificate must be checked or not
     * @param bool $sslVerify
     * @return self
     */
    public function setSslVerify($sslVerify);

    /**
     * Set whether or not to follow redirections
     * @param bool $followRedirections
     * @return self
     */
    public function setFollowRedirections($followRedirections);

    /**
     * Set the port to use
     * @param int $port
     * @return self
     */
    public function setPort($port);

    /**
     * Set the headers to be sent
     * @param string[] $headers
     * @return self
     */
    public function setHeaders($headers);

    /**
     * Set socks proxy URL
     * @param string $socksProxyUrl
     * @return self
     */
    public function setSocksProxyUrl($socksProxyUrl);

    /**
     * Set socks proxy port
     * @param inr $socksProxyPort
     * @return self
     */
    public function setSocksProxyPort($socksProxyPort);
}