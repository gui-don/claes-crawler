<?php

namespace Claes\Crawler;

use Claes\Exception\FileException;
use Claes\Crawler\Interfaces\HttpResponseInterface;
use Claes\Crawler\Interfaces\HttpResponseHeaderInterface;
use Rico\Lib\File;
use Rico\Lib\Chars;

/**
 * HttpResponse class
 */
class HttpResponse implements HttpResponseInterface
{
    /**
     * Response content
     * @var string
     */
    protected $content;
    /**
     * Response MIME type
     * @var string
     */
    protected $mime = 'plain/text';
    /**
     * HTTP response code
     * @var int
     */
    protected $code;
    /**
     * HTTP response headers object
     * @var HttpResponseHeaderInterface
     */
    protected $headers;

    /**
     * {@inheritdoc}
     */
    public function __construct($content, $code, $rawHeaders, HttpResponseHeaderInterface $httpResponseHeader)
    {
        $httpResponseHeader->hydrate($rawHeaders);
        $this->setContent($content);
        $this->setCode($code);
        $this->setHeaders($httpResponseHeader);
        $this->setMime($httpResponseHeader->getContentType());
    }

    /**
     * {@inheritdoc}
     */
    public function save($filename, $override = true)
    {
        if(!is_string($filename)) {
            return false;
        }

        // Create a filename id needed
        if (is_dir($filename)) {
            $filename .= md5($this->getContent()).'.'.Chars::getResourceNameInUrl($this->getMime());
        }

        // File already exists, without override
        if (file_exists($filename) && !$override) {
            throw new FileException('Cannot save the downloaded content to “'.$filename.'”. Target already exist! Use override=true to ignore.');
        }

        // If the content already exist, with override
        if (file_exists($filename) && $override) {
            unlink($filename);
        }

        // Create path if it does not exist
        File::createPath(dirname($filename));

        $saveContentHandler = fopen($filename, 'w');
        fwrite($saveContentHandler, $this->getContent());
        fclose($saveContentHandler);

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * {@inheritdoc}
     */
    public function getMime()
    {
        return $this->mime;
    }

    /**
     * {@inheritdoc}
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * {@inheritdoc}
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * {@inheritdoc}
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setMime($mime)
    {
        if (is_string($mime)) {
            $this->mime = trim(explode(';', $mime)[0]);
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setHeaders(HttpResponseHeaderInterface $headers)
    {
        $this->headers = $headers;
        return $this;
    }

}