<?php

namespace Claes\Crawler;

use Claes\Crawler\HttpHeader;
use Claes\Crawler\Interfaces\HttpResponseHeaderInterface;

class HttpResponseHeader extends HttpHeader implements HttpResponseHeaderInterface
{
    protected $headers = [
        'access-control-allow-origin' => ['Access-Control-Allow-Origin', ''],
        'accept-patch' => ['Accept-Patch', ''],
        'accept-ranges' => ['Accept-Ranges', ''],
        'age' => ['Age', ''],
        'allow' => ['Allow', ''],
        'cache-control' => ['Cache-Control', ''],
        'connection' => [ 'Connection', ''],
        'content-disposition' => ['Content-Disposition', ''],
        'content-encoding' => ['Content-Encoding', ''],
        'content-language' => ['Content-Language', ''],
        'content-length' => ['Content-Length', ''],
        'content-location' => ['Content-Location', ''],
        'content-md5' => ['Content-MD5', ''],
        'content-range' => ['Content-Range', ''],
        'content-type' => ['Content-Type', ''],
        'date' => ['Date', ''],
        'etag' => ['ETag', ''],
        'expires' => ['Expires', ''],
        'last-modified' => ['Last-Modified', ''],
        'link' => ['Link', ''],
        'location' => ['Location', ''],
        'p3p' => ['P3P', ''],
        'pragma' => ['Pragma', ''],
        'proxy-authenticate' => ['Proxy-Authenticate', ''],
        'public-key-pins' => ['Public-Key-Pins', ''],
        'refresh' => ['Refresh', ''],
        'retry-after' => ['Retry-After', ''],
        'server' => ['Server', ''],
        'set-cookie' => ['Set-Cookie', ''],
        'status' => ['Status', ''],
        'strict-transport-security' => ['Strict-Transport-Security', ''],
        'trailer' => ['Trailer', ''],
        'transfer-encoding' => ['Transfer-Encoding', ''],
        'upgrade' => ['Upgrade', ''],
        'vary' => ['Vary', ''],
        'via' => ['Via', ''],
        'warning' => ['Warning', ''],
        'www-authenticate' => ['WWW-Authenticate', ''],
        'x-frame-options' => ['X-Frame-Options', ''],
        'x-xss-protection' => ['X-XSS-Protection', ''],
        'content-security-policy' => ['Content-Security-Policy', ''],
        'x-content-type-options' => ['X-Content-Type-Options', ''],
        'x-powered-by' => ['X-Powered-By', ''],
        'x-ua-compatible' => ['X-UA-Compatible', ''],
        'x-content-duration' => ['X-Content-Duration', ''],
    ];

    public function getAccessControlAllowOrigin()
    {
        return $this->headers['access-control-allow-origin'][1];
    }

    public function getAcceptPatch()
    {
        return $this->headers['accept-patch'][1];
    }

    public function getAcceptRanges()
    {
        return $this->headers['accept-ranges'][1];
    }

    public function getAge()
    {
        return $this->headers['age'][1];
    }

    public function getAllow()
    {
        return $this->headers['allow'][1];
    }

    public function getCacheControl()
    {
        return $this->headers['cache-control'][1];
    }

    public function getConnection()
    {
        return $this->headers['connection'][1];
    }

    public function getContentDisposition()
    {
        return $this->headers['content-disposition'][1];
    }

    public function getContentEncoding()
    {
        return $this->headers['content-encoding'][1];
    }

    public function getContentLanguage()
    {
        return $this->headers['content-language'][1];
    }

    public function getContentLength()
    {
        return $this->headers['content-length'][1];
    }

    public function getContentLocation()
    {
        return $this->headers['location'][1];
    }

    public function getContentMd5()
    {
        return $this->headers['content-md5'][1];
    }

    public function getContentRange()
    {
        return $this->headers['content-range'][1];
    }

    public function getContentType()
    {
        return $this->headers['content-type'][1];
    }

    public function getDate()
    {
        return $this->headers['date'][1];
    }

    public function getETag()
    {
        return $this->headers['etag'][1];
    }

    public function getExpires()
    {
        return $this->headers['expires'][1];
    }

    public function getLastModified()
    {
        return $this->headers['last-modified'][1];
    }

    public function getLink()
    {
        return $this->headers['link'][1];
    }

    public function getLocation()
    {
        return $this->headers['location'][1];
    }

    public function getP3p()
    {
        return $this->headers['p3p'][1];
    }

    public function getPragma()
    {
        return $this->headers['pragma'][1];
    }

    public function getProxyAuthenticate()
    {
        return $this->headers['proxy-authenticate'][1];
    }

    public function getPublicKeyPins()
    {
        return $this->headers['public-key-pins'][1];
    }

    public function getRefresh()
    {
        return $this->headers['refresh'][1];
    }

    public function getRetryAfter()
    {
        return $this->headers['retry-after'][1];
    }

    public function getServer()
    {
        return $this->headers['server'][1];
    }

    public function getSetCookie()
    {
        return $this->headers['set-cookie'][1];
    }

    public function getStatus()
    {
        return $this->headers['status'][1];
    }

    public function getStrictTransportSecurity()
    {
        return $this->headers['strict-transport-security'][1];
    }

    public function getTrailer()
    {
        return $this->headers['trailer'][1];
    }

    public function getTransferEncoding()
    {
        return $this->headers['transfer-encoding'][1];
    }

    public function getUpdrade()
    {
        return $this->headers['upgrade'][1];
    }

    public function getVary()
    {
        return $this->headers['vary'][1];
    }

    public function getVia()
    {
        return $this->headers['via'][1];
    }

    public function getWarning()
    {
        return $this->headers['warning'][1];
    }

    public function getWWWAutenticate()
    {
        return $this->headers['www-authenticate'][1];
    }

    public function getXFrameOptions()
    {
        return $this->headers['x-frame-options'][1];
    }

    public function getXXSSProtection()
    {
        return $this->headers['x-xss-protection'][1];
    }

    public function getContentSecurityPolicy()
    {
        return $this->headers['content-security-policy'][1];
    }

    public function getContentTypeOptions()
    {
        return $this->headers['content-type-options'][1];
    }

    public function getXPoweredBy()
    {
        return $this->headers['x-powered-by'][1];
    }

    public function getXUACompatible()
    {
        return $this->headers['x-ua-compatible'][1];
    }

    public function getContentDuration()
    {
        return $this->headers['x-content-duration'][1];
    }

    public function setAccessControlAllowOrigin($accessControlAllowOrigin)
    {
        $header = $this->headers['access-control-allow-origin'];
        $header[1] = $accessControlAllowOrigin;
        unset($this->headers['access-control-allow-origin']);
        $this->headers['access-control-allow-origin'] = $header;
        return $this;
    }

    public function setAcceptPatch($acceptPatch)
    {
        $header = $this->headers['accept-path'];
        $header[1] = $acceptPatch;
        unset($this->headers['accept-path']);
        $this->headers['accept-path'] = $header;
        return $this;
    }

    public function setAcceptRanges($acceptRanges)
    {
        $header = $this->headers['accept-ranges'];
        $header[1] = $acceptRanges;
        unset($this->headers['accept-ranges']);
        $this->headers['accept-ranges'] = $header;
        return $this;
    }

    public function setAge($age)
    {
        $header = $this->headers['age'];
        $header[1] = $age;
        unset($this->headers['age']);
        $this->headers['age'] = $header;
        return $this;
    }

    public function setAllow($allow)
    {
        $header = $this->headers['allow'];
        $header[1] = $allow;
        unset($this->headers['allow']);
        $this->headers['allow'] = $header;
        return $this;
    }

    public function setCacheControl($cacheControl)
    {
        $header = $this->headers['cache-control'];
        $header[1] = $cacheControl;
        unset($this->headers['cache-control']);
        $this->headers['cache-control'] = $header;
        return $this;
    }

    public function setConnection($connection)
    {
        $header = $this->headers['connection'];
        $header[1] = $connection;
        unset($this->headers['connection']);
        $this->headers['connection'] = $header;
        return $this;
    }

    public function setContentDisposition($contentDisposition)
    {
        $header = $this->headers['content-disposition'];
        $header[1] = $contentDisposition;
        unset($this->headers['content-disposition']);
        $this->headers['content-disposition'] = $header;
        return $this;
    }

    public function setContentEncoding($contentEncoding)
    {
        $header = $this->headers['content-encoding'];
        $header[1] = $contentEncoding;
        unset($this->headers['content-encoding']);
        $this->headers['content-encoding'] = $header;
        return $this;
    }

    public function setContentLanguage($contentLanguage)
    {
        $header = $this->headers['content-language'];
        $header[1] = $contentLanguage;
        unset($this->headers['content-language']);
        $this->headers['content-language'] = $header;
        return $this;
    }

    public function setContentLength($contentLength)
    {
        $header = $this->headers['content-length'];
        $header[1] = $contentLength;
        unset($this->headers['content-length']);
        $this->headers['content-length'] = $header;
        return $this;
    }

    public function setContentLocation($contentLocation)
    {
        $header = $this->headers['location'];
        $header[1] = $contentLocation;
        unset($this->headers['location']);
        $this->headers['location'] = $header;
        return $this;
    }

    public function setContentMd5($contentMd5)
    {
        $header = $this->headers['content-md5'];
        $header[1] = $contentMd5;
        unset($this->headers['content-md5']);
        $this->headers['content-md5'] = $header;
        return $this;
    }

    public function setContentRange($contentRange)
    {
        $header = $this->headers['content-range'];
        $header[1] = $contentRange;
        unset($this->headers['content-range']);
        $this->headers['content-range'] = $header;
        return $this;
    }

    public function setContentType($contentType)
    {
        $header = $this->headers['content-type'];
        $header[1] = $contentType;
        unset($this->headers['content-type']);
        $this->headers['content-type'] = $header;
        return $this;
    }

    public function setDate($date)
    {
        $header = $this->headers['date'];
        $header[1] = $date;
        unset($this->headers['date']);
        $this->headers['date'] = $header;
        return $this;
    }

    public function setETag($eTag)
    {
        $header = $this->headers['etag'];
        $header[1] = $eTag;
        unset($this->headers['etag']);
        $this->headers['etag'] = $header;
        return $this;
    }

    public function setExpires($expires)
    {
        $header = $this->headers['expires'];
        $header[1] = $expires;
        unset($this->headers['expires']);
        $this->headers['expires'] = $header;
        return $this;
    }

    public function setLastModified($lastModified)
    {
        $header = $this->headers['last-modified'];
        $header[1] = $lastModified;
        unset($this->headers['last-modified']);
        $this->headers['last-modified'] = $header;
        return $this;
    }

    public function setLink($link)
    {
        $header = $this->headers['link'];
        $header[1] = $link;
        unset($this->headers['link']);
        $this->headers['link'] = $header;
        return $this;
    }

    public function setLocation($location)
    {
        $header = $this->headers['location'];
        $header[1] = $location;
        unset($this->headers['location']);
        $this->headers['location'] = $header;
        return $this;
    }

    public function setP3p($p3p)
    {
        $header = $this->headers['p3p'];
        $header[1] = $p3p;
        unset($this->headers['p3p']);
        $this->headers['p3p'] = $header;
        return $this;
    }

    public function setPragma($pragma)
    {
        $header = $this->headers['pragma'];
        $header[1] = $pragma;
        unset($this->headers['pragma']);
        $this->headers['pragma'] = $header;
        return $this;
    }

    public function setProxyAuthenticate($proxyAuthetiate)
    {
        $header = $this->headers['proxy-authenticate'];
        $header[1] = $proxyAuthetiate;
        unset($this->headers['proxy-authenticate']);
        $this->headers['proxy-authenticate'] = $header;
        return $this;
    }

    public function setPublicKeyPins($publicKeyPins)
    {
        $header = $this->headers['public-key-pins'];
        $header[1] = $publicKeyPins;
        unset($this->headers['public-key-pins']);
        $this->headers['public-key-pins'] = $header;
        return $this;
    }

    public function setRefresh($refresh)
    {
        $header = $this->headers['refresh'];
        $header[1] = $refresh;
        unset($this->headers['refresh']);
        $this->headers['refresh'] = $header;
        return $this;
    }

    public function setRetryAfter($retryAfter)
    {
        $header = $this->headers['retry-after'];
        $header[1] = $retryAfter;
        unset($this->headers['retry-after']);
        $this->headers['retry-after'] = $header;
        return $this;
    }

    public function setServer($server)
    {
        $header = $this->headers['server'];
        $header[1] = $server;
        unset($this->headers['server']);
        $this->headers['server'] = $header;
        return $this;
    }

    public function setSetCookie($setCookie)
    {
        $header = $this->headers['set-cookie'];
        $header[1] = $setCookie;
        unset($this->headers['set-cookie']);
        $this->headers['set-cookie'] = $header;
        return $this;
    }

    public function setStatus($status)
    {
        $header = $this->headers['status'];
        $header[1] = $status;
        unset($this->headers['status']);
        $this->headers['status'] = $header;
        return $this;
    }

    public function setStrictTransportSecurity($strictTransportSecurity)
    {
        $header = $this->headers['strict-transport-security'];
        $header[1] = $strictTransportSecurity;
        unset($this->headers['strict-transport-security']);
        $this->headers['strict-transport-security'] = $header;
        return $this;
    }

    public function setTrailer($trailer)
    {
        $header = $this->headers['trailer'];
        $header[1] = $trailer;
        unset($this->headers['trailer']);
        $this->headers['trailer'] = $header;
        return $this;
    }

    public function setTransferEncoding($transferEncoding)
    {
        $header = $this->headers['transfer-encoding'];
        $header[1] = $transferEncoding;
        unset($this->headers['transfer-encoding']);
        $this->headers['transfer-encoding'] = $header;
        return $this;
    }

    public function setUpdrade($updrade)
    {
        $header = $this->headers['upgrade'];
        $header[1] = $updrade;
        unset($this->headers['upgrade']);
        $this->headers['upgrade'] = $header;
        return $this;
    }

    public function setVary($vary)
    {
        $header = $this->headers['vary'];
        $header[1] = $vary;
        unset($this->headers['vary']);
        $this->headers['vary'] = $header;
        return $this;
    }

    public function setVia($via)
    {
        $header = $this->headers['vary'];
        $header[1] = $via;
        unset($this->headers['vary']);
        $this->headers['vary'] = $header;
        return $this;
    }

    public function setWarning($warning)
    {
        $header = $this->headers['warning'];
        $header[1] = $warning;
        unset($this->headers['warning']);
        $this->headers['warning'] = $header;
        return $this;
    }

    public function setWWWAutenticate($wWWAutenticate)
    {
        $header = $this->headers['www-authenticate'];
        $header[1] = $wWWAutenticate;
        unset($this->headers['www-authenticate']);
        $this->headers['www-authenticate'] = $header;
        return $this;
    }

    public function setXFrameOptions($xFrameOptions)
    {
        $header = $this->headers['x-frame-options'];
        $header[1] = $xFrameOptions;
        unset($this->headers['x-frame-options']);
        $this->headers['x-frame-options'] = $header;
        return $this;
    }

    public function setXXSSProtection($xXSSProtection)
    {
        $header = $this->headers['x-xss-protection'];
        $header[1] = $xXSSProtection;
        unset($this->headers['x-xss-protection']);
        $this->headers['x-xss-protection'] = $header;
        return $this;
    }

    public function setContentSecurityPolicy($contentSecurityPolicy)
    {
        $header = $this->headers['content-security-policy'];
        $header[1] = $contentSecurityPolicy;
        unset($this->headers['content-security-policy']);
        $this->headers['content-security-policy'] = $header;
        return $this;
    }

    public function setContentTypeOptions($contentTypeOptions)
    {
        $header = $this->headers['content-type-options'];
        $header[1] = $contentTypeOptions;
        unset($this->headers['content-type-options']);
        $this->headers['content-type-options'] = $header;
        return $this;
    }

    public function setXPoweredBy($xPoweredBy)
    {
        $header = $this->headers['x-powered-by'];
        $header[1] = $xPoweredBy;
        unset($this->headers['x-powered-by']);
        $this->headers['x-powered-by'] = $header;
        return $this;
    }

    public function setXUACompatible($xUACompatible)
    {
        $header = $this->headers['x-ua-compatible'];
        $header[1] = $xUACompatible;
        unset($this->headers['x-ua-compatible']);
        $this->headers['x-ua-compatible'] = $header;
        return $this;
    }

    public function setContentDuration($contentDuration)
    {
        $header = $this->headers['x-content-duration'];
        $header[1] = $contentDuration;
        unset($this->headers['x-content-duration']);
        $this->headers['x-content-duration'] = $header;
        return $this;
    }
}
