<?php

namespace Claes\Crawler;

use Claes\Crawler\Interfaces\HttpEngineInterface;
use Claes\Exception\CurlGetException;
use Claes\Exception\CurlUrlException;
use Rico\Lib\Checker;

class CurlEngine implements HttpEngineInterface
{
    /**
     * Curl resource
     * @var resource
     */
    private $curlResource;

    /**
     * Raw response header
     * @var string
     */
    private $responseHeaders = '';

    /**
     * Raw response body
     * @var string
     */
    private $responseBody = '';

    /**
     * Response code
     * @var int
     */
    private $responseCode;

    /**
     * Url to handle
     * @var string
     */
    private $url;

    /**
     * HTTP version to use
     * @var float
     */
    private $httpVersion = CURL_HTTP_VERSION_1_1;

    /**
     * SSL version to use
     * @var float
     */
    private $sslVersion = CURL_SSLVERSION_TLSv1_2;

    /**
     * Determine if SSL certificate must be checked or not
     * @var int
     */
    private $sslVerify = true;

    /**
     * Determine whether or not to follow redirections
     * @var bool
     */
    private $followRedirections = true;

    /**
     * The port to use
     * @var int
     */
    private $port;

    /**
     * Headers to be sent
     * @var string[]
     */
    private $headers;

    /**
     * Socks proxy URL
     * @var string
     */
    private $socksProxyUrl;

    /**
     * Socks proxy port
     * @var int
     */
    private $socksProxyPort = 80;

    /**
     * {@inheritdoc}
     */
    public function __construct($url)
    {
        $this->curlResource = curl_init();
        $this->setDefaultOptions();
        $this->setUrl($url);
    }

    /**
     * {@inheritdoc}
     */
    public function __destruct()
    {
        curl_close($this->curlResource);
    }

    public function __toString()
    {
        return $this->getUrl();
    }

    /**
     * {@inheritdoc}
     */
    public function get()
    {
        if (!$this->curlResource) {
            throw new CurlUrlException('Can’t make the GET request. Curl was not initialised.');
        }

        if ($rawResponse = curl_exec($this->curlResource)) {
            $headerSize = curl_getinfo($this->curlResource, CURLINFO_HEADER_SIZE);
            $this->responseHeaders = substr($rawResponse, 0, $headerSize);
            $this->responseBody = substr($rawResponse, $headerSize);
            $this->responseCode = curl_getinfo($this->curlResource, CURLINFO_HTTP_CODE);
        } else {
            throw new CurlGetException('Curl failed to GET “'.$this->getUrl().'”. with error “'.curl_error($this->curlResource).'”');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseHeaders()
    {
        return $this->responseHeaders;
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseBody()
    {
        return $this->responseBody;
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseCode()
    {
        return $this->responseCode;
    }

    /**
     * {@inheritdoc}
     */
    public function getTransactionInformation()
    {
        return [
            'effective_url' => curl_getinfo($this->curlResource, CURLINFO_EFFECTIVE_URL),
            'http_code' => curl_getinfo($this->curlResource, CURLINFO_HTTP_CODE),
            'filetime' => curl_getinfo($this->curlResource, CURLINFO_FILETIME),
            'total_time' => curl_getinfo($this->curlResource, CURLINFO_TOTAL_TIME),
            'namelookup_time' => curl_getinfo($this->curlResource, CURLINFO_NAMELOOKUP_TIME),
            'connect_time' => curl_getinfo($this->curlResource, CURLINFO_CONNECT_TIME),
            'pretransfer_time' => curl_getinfo($this->curlResource, CURLINFO_PRETRANSFER_TIME),
            'starttransfer_time' => curl_getinfo($this->curlResource, CURLINFO_STARTTRANSFER_TIME),
            'redirect_count' => curl_getinfo($this->curlResource, CURLINFO_REDIRECT_COUNT),
            'redirect_time' => curl_getinfo($this->curlResource, CURLINFO_REDIRECT_TIME),
            'redirect_url' => curl_getinfo($this->curlResource, CURLINFO_REDIRECT_URL),
            'primary_ip' => curl_getinfo($this->curlResource, CURLINFO_PRIMARY_IP),
            'primary_port' => curl_getinfo($this->curlResource, CURLINFO_PRIMARY_PORT),
            'local_ip' => curl_getinfo($this->curlResource, CURLINFO_LOCAL_IP),
            'local_port' => curl_getinfo($this->curlResource, CURLINFO_LOCAL_PORT),
            'size_upload' => curl_getinfo($this->curlResource, CURLINFO_SIZE_UPLOAD),
            'size_download' => curl_getinfo($this->curlResource, CURLINFO_SIZE_DOWNLOAD),
            'speed_upload' => curl_getinfo($this->curlResource, CURLINFO_SPEED_UPLOAD),
            'speed_download' => curl_getinfo($this->curlResource, CURLINFO_SPEED_DOWNLOAD),
            'header_out' => curl_getinfo($this->curlResource, CURLINFO_HEADER_OUT),
            'ssl_verifyresult' => curl_getinfo($this->curlResource, CURLINFO_SSL_VERIFYRESULT),
        ];
    }

    /**
     * Get internal CuRL resource
     * @return resource Curl resource
     */
    public function getCurlResource()
    {
        return $this->curlResource;
    }

    /**
     * {@inheritdoc}
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * {@inheritdoc}
     */
    public function getHttpVersion()
    {
        return $this->httpVersion;
    }

    /**
     * {@inheritdoc}
     */
    public function getSslVersion()
    {
        return $this->sslVersion;
    }

    /**
     * {@inheritdoc}
     */
    public function getSslVerify()
    {
        return $this->sslVerify;
    }

    /**
     * {@inheritdoc}
     */
    public function getFollowRedirections()
    {
        return $this->followRedirections;
    }

    /**
     * {@inheritdoc}
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * {@inheritdoc}
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * {@inheritdoc}
     */
    public function getSocksProxyUrl()
    {
        return $this->socksProxyUrl;
    }

    /**
     * {@inheritdoc}
     */
    public function getSocksProxyPort()
    {
        return $this->socksProxyPort;
    }

    /**
     * {@inheritdoc}
     */
    public function setUrl($url)
    {
        if (!Checker::isURL($url)) {
            throw new CurlUrlException('Curl failed to initialise with URL “'.$url.'”. URL is not valid.');
        }

        $this->url = $url;
        curl_setopt($this->curlResource, CURLOPT_URL, $this->getUrl());
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setHttpVersion($httpVersion)
    {
        $this->httpVersion = $httpVersion;
        curl_setopt($this->curlResource, CURLOPT_HTTP_VERSION, $this->getHttpVersion());
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setSslVersion($sslVersion)
    {
        $this->sslVersion = $sslVersion;
        curl_setopt($this->curlResource, CURLOPT_SSLVERSION, $this->getSslVersion());
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setSslVerify($sslVerify)
    {
        $this->sslVerify = $sslVerify;
        curl_setopt($this->curlResource, CURLOPT_SSL_VERIFYPEER, $this->getSslVerify());

        if ($this->getSslVerify()) {
            curl_setopt($this->curlResource, CURLOPT_SSL_VERIFYHOST, 2);
        } else {
            curl_setopt($this->curlResource, CURLOPT_SSL_VERIFYHOST, 0);
        }
        
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setFollowRedirections($followRedirections)
    {
        $this->followRedirections = $followRedirections;
        curl_setopt($this->curlResource, CURLOPT_FOLLOWLOCATION, $this->getFollowRedirections());
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setPort($port)
    {
        $this->port = $port;
        curl_setopt($this->curlResource, CURLOPT_PORT, $this->getPort());
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;
        curl_setopt($this->curlResource, CURLOPT_HTTPHEADER, $this->getHeaders());
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setSocksProxyUrl($socksProxyUrl)
    {
        if (is_string($socksProxyUrl)) {
            $this->socksProxyUrl = $socksProxyUrl;
            curl_setopt($this->curlResource, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
            curl_setopt($this->curlResource, CURLOPT_PROXYPORT, $this->getSocksProxyPort());
            curl_setopt($this->curlResource, CURLOPT_PROXY, $this->getSocksProxyUrl());
        } else {
            $this->socksProxyUrl = '';
            curl_setopt($this->curlResource, CURLOPT_PROXYTYPE, null);
            curl_setopt($this->curlResource, CURLOPT_PROXYPORT, null);
            curl_setopt($this->curlResource, CURLOPT_PROXY, null);
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setSocksProxyPort($socksProxyPort)
    {
        $this->socksProxyPort = $socksProxyPort;
        curl_setopt($this->curlResource, CURLOPT_PROXYPORT, $this->getSocksProxyPort());
        return $this;
    }

    /**
     * Set the default options
     */
    private function setDefaultOptions()
    {
        curl_setopt_array($this->curlResource, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HEADER => 1,
            CURLINFO_HEADER_OUT => 1,
            CURLOPT_AUTOREFERER => false,
            CURLOPT_DNS_CACHE_TIMEOUT => 3600,
            CURLOPT_HTTP_VERSION => $this->getHttpVersion(),
            CURLOPT_SSLVERSION => $this->getSslVersion(),
            CURLOPT_FOLLOWLOCATION => $this->getFollowRedirections(),
        ]);
    }
}