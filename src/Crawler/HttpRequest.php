<?php

namespace Claes\Crawler;

use Claes\Core\Interfaces\ContainerInterface;
use Claes\Crawler\Interfaces\HttpRequestInterface;
use Claes\Crawler\Interfaces\HttpRequestHeaderInterface;
use Claes\Crawler\Interfaces\HttpEngineInterface;
use Claes\Exception\DownloadException;
use Claes\Exception\ResponseException;
use Claes\Exception\CurlException;
use Claes\Exception\RequestException;

/**
 * Handles HTTP requests
 */
class HttpRequest implements HttpRequestInterface
{
    const STATUS_NOT_SEND = 0;
    const STATUS_SEND = 1;

    /**
     * Status of the request
     * @var bool
     */
    protected $status = self::STATUS_NOT_SEND;

    /**
     * Request headers
     * @var HttpRequestHeaderInterface
     */
    protected $headers;

    /**
     * HTTP Engine
     * @var HttpEngineInterface
     */
    protected $httpEngine;

    /**
     * Container of services
     * @var ContainerInterface
     */
    protected $container;

    /**
     * {@inheritdoc}
     */
    public function __construct(HttpRequestHeaderInterface $requestHeaders, HttpEngineInterface $httpEngine, ContainerInterface $container)
    {
        $this->setHeaders($requestHeaders);
        $this->httpEngine = $httpEngine;
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function send()
    {
        try {
            $this->httpEngine->setHeaders($this->getHeaders()->convertCurl());
            $this->httpEngine->get();
            $response = $this->container->getResponse($this->httpEngine->getResponseBody(), $this->httpEngine->getResponseCode(), $this->httpEngine->getResponseHeaders());
        } catch (CurlException $ex) {
            throw new DownloadException('Can’t fullfill the request.', 0, $ex);
        } catch (\Exception $ex) {
            throw new ResponseException('Response is not valid.', 0, $ex);
        }

        if ($response->getHeaders()->getContentEncoding() === 'gzip') {
            $response->setContent(gzdecode($response->getContent()));
        } else if ($response->getHeaders()->getContentEncoding() === 'deflate') {
            $response->setContent(gzinflate($response->getContent()));
        }

        $this->setStatus(self::STATUS_SEND);

        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function changeUrl($url)
    {
        try {
            $this->httpEngine->setUrl($url);
        } catch (CurlException $ex) {
            throw new RequestException('The HTTP request cannot be change to URL “'.$url.'”.', 0, $ex);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * {@inheritdoc}
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * {@inheritdoc}
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * {@inheritdoc}
     */
    public function getHttpEngine()
    {
        return $this->httpEngine;
    }

    /**
     * {@inheritdoc}
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setHeaders(HttpRequestHeaderInterface $headers)
    {
        $this->headers = $headers;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setHttpEngine(HttpEngineInterface $httpEngine)
    {
        $this->httpEngine = $httpEngine;
        return $this;
    }

}
