<?php

namespace Claes\Crawler;

use \Claes\Crawler\Interfaces\DomParserInterface;
use Rico\Lib\Chars;
use Symfony\Component\CssSelector\CssSelectorConverter;

/**
 * DomDocument and DomXPath wrapper
 */
class DomParser extends \DOMDocument implements DomParserInterface
{
    /**
     * DomXPath object
     * @var \DOMXPath
     */
    protected $domXPath;

    /**
     * {@inheritDoc}
     */
    public function __construct($rawBody)
    {
        parent::__construct();

        $this->setContent($rawBody);
    }

    /**
     * {@inheritDoc}
     */
    public function setContent($rawBody)
    {
        @$this->loadHTML('<?xml encoding="UTF-8">'.$rawBody);
        $this->domXPath = new \DomXPath($this);
    }

    /**
     * {@inheritDoc}
     */
    public function getFirstValueByTagName($tagName, $regExtract = null)
    {
        if (!is_string($tagName)) {
            return false;
        }

        foreach ($this->getElementsByTagName($tagName) as $node) {
            if (empty($regExtract)) {
                return Chars::normalize($node->nodeValue);
            } else {
                return $this->regExtract($regExtract, Chars::normalize($node->nodeValue));
            }
        }

        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function getFirstValueById($id, $regExtract = null)
    {
        if (!is_string($id)) {
            return false;
        }

        $extract = $this->getElementById($id);
        if (!is_null($extract)) {
            if (empty($regExtract)) {
                return Chars::normalize($extract->nodeValue);
            } else {
                return $this->regExtract($regExtract, Chars::normalize($extract->nodeValue));
            }
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getFirstValueByClass($class, $regExtract = null)
    {
        if (!is_string($class)) {
            return false;
        }

        foreach ($this->domXPath->query("//*[@class='$class' or contains(@class, ' $class') or contains(@class, '$class ')]") as $node) {
            if (empty($regExtract)) {
                return Chars::normalize($node->nodeValue);
            } else {
                return $this->regExtract($regExtract, Chars::normalize($node->nodeValue));
            }
        }

        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function getFirstValueByCss($css, $regExtract = null)
    {
        if (!is_string($css)) {
            return false;
        }

        $cssSelectorConverter = new CssSelectorConverter();
        $xPathExpression = $cssSelectorConverter->toXPath($css);

        foreach ($this->domXPath->query($xPathExpression) as $node) {
            if (empty($regExtract)) {
                return Chars::normalize($node->nodeValue);
            } else {
                return $this->regExtract($regExtract, Chars::normalize($node->nodeValue));
            }
        }

        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function getAllUrlInsideCss($css)
    {
        if (!is_string($css)) {
            return false;
        }

        $cssSelectorConverter = new CssSelectorConverter();
        $xPathExpression = $cssSelectorConverter->toXPath($css);

        $urls = [];

        foreach ($this->domXPath->query($xPathExpression) as $node) {
            if (!empty($node->getAttribute('src'))) {
                $urls[] = $node->getAttribute('src');
            } else if (!empty($node->getAttribute('href'))) {
                $urls[] = $node->getAttribute('href');
            }

            foreach ($node->getElementsByTagName('a') as $image) {
                if (!empty($image->getAttribute('href'))) {
                    $urls[] = $image->getAttribute('href');
                }
            }

            foreach ($node->getElementsByTagName('img') as $image) {
                if (!empty($image->getAttribute('src'))) {
                    $urls[] = $image->getAttribute('src');
                }
            }
        }

        return $urls;
    }

    /**
     * {@inheritDoc}
     */
    public function slimDomParser($css)
    {
        if (!is_string($css)) {
            return false;
        }

        $cssSelectorConverter = new CssSelectorConverter();
        $xPathExpression = $cssSelectorConverter->toXPath($css);

        $parsers = [];

        foreach ($this->domXPath->query($xPathExpression) as $node) {
            $parsers[] = new self($this->saveXML($node));
        }

        return $parsers;
    }

    /**
     * {@inheritDoc}
     */
    public function getDomXPath()
    {
        return $this->domXPath;
    }

    /**
     * Extract a value from string with a regular expression
     * @param string $regexp Regular expression
     * @param string $value The main string to apply the regular expression
     * @return string The found string or false if regexp is invalid or the $value string if nothing is found
     */
    private function regExtract($regexp, $value)
    {
        preg_match($regexp, $value, $matches);
        if (!empty($matches[1])) {
            return $matches[1];
        } else {
            return $value;
        }
    }
}
