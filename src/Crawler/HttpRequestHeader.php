<?php

namespace Claes\Crawler;

use Claes\Crawler\HttpHeader;
use Claes\Crawler\Interfaces\HttpRequestHeaderInterface;

class HttpRequestHeader extends HttpHeader implements HttpRequestHeaderInterface
{
    protected $headers = [
        'accept' => ['Accept', 'application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5'],
        'accept-charset' => ['Accept-Charset', 'utf-8'],
        'accept-encoding' => ['Accept-Encoding', 'gzip, deflate'],
        'accept-language' => ['Accept-Language', 'en'],
        'accept-datetime' => ['Accept-Datetime', ''],
        'authorization' => ['Authorization', ''],
        'cache-control' => ['Cache-Control', 'max-age=0'],
        'connection' => ['Connection', 'keep-alive'],
        'cookie' => ['Cookie', ''],
        'content-length' => ['Content-Length', ''],
        'content-md5' => ['Content-MD5', ''],
        'content-type' => ['Content-Type', ''],
        'date' => ['Date', ''],
        'expect' => ['Expect', ''],
        'from' => ['From', ''],
        'host' => ['Host', ''],
        'if-match' => ['If-Match', ''],
        'if-modified-since' => ['If-Modified-Since', ''],
        'if-none-match' => ['If-None-Match', ''],
        'if-range' => ['If-Range', ''],
        'if-unmodified-since' => ['If-Unmodified-Since', ''],
        'max-forwards' => ['Max-Forwards', ''],
        'origin' => ['Origin', ''],
        'pragma' => ['Pragma', ''],
        'proxy-authorization' => ['Proxy-Authorization', ''],
        'range' => ['Range', ''],
        'referer' => ['Referer', ''],
        'te' => ['TE', ''],
        'user-agent' => ['User-Agent', 'Mozilla/5.0 (Windows NT 6.1; rv:41.0) Gecko/20100101 Firefox/41.0'],
        'upgrade' => ['Upgrade', ''],
        'via' => ['Via', ''],
        'warning' => ['Warning', ''],
        'x-requested-with' => ['X-Requested-With', ''],
        'dnt' => ['DNT', ''],
        'x-forwarded-for' => ['X-Forwarded-For', ''],
        'x-forwarded-host' => ['X-Forwarded-Host', ''],
        'x-forwarded-proto' => ['X-Forwarded-Proto', ''],
        'x-http-method-override' => ['X-Http-Method-Override', ''],
        'x-wap-profile' => ['X-Wap-Profile', ''],
        'proxy-connection' => ['Proxy-Connection', ''],
        'x-csrf-token' => ['X-Csrf-Token', ''],
    ];

    protected $accept;
    protected $acceptCharset;
    protected $acceptEncoding;
    protected $acceptLanguage;
    protected $acceptDatetime;
    protected $authorization;
    protected $cacheControl;
    protected $connection;
    protected $cookie;
    protected $contentLength;
    protected $contentMd5;
    protected $contentType;
    protected $date;
    protected $expect;
    protected $from;
    protected $host;
    protected $ifMatch;
    protected $ifModifiedSince;
    protected $ifNoneMatch;
    protected $ifRange;
    protected $ifUnmodifiedSince;
    protected $maxForwards;
    protected $origin;
    protected $pragma;
    protected $proxyAuthorization;
    protected $range;
    protected $referer;
    protected $te;
    protected $userAgent;
    protected $upgrade;
    protected $via;
    protected $warning;
    protected $xRequestedWith;
    protected $dnt;
    protected $xForwardedFor;
    protected $xForwardedHost;
    protected $xForwardedProto;
    protected $xHttpMethodOverride;
    protected $xWapProfile;
    protected $proxyConnection;
    protected $xCsrfToken;

    public function getAccept()
    {
        return $this->headers['accept'][1];
    }

    public function getAcceptCharset()
    {
        return $this->headers['accept-charset'][1];
    }

    public function getAcceptEncoding()
    {
        return $this->headers['accept-encoding'][1];
    }

    public function getAcceptLanguage()
    {
        return $this->headers['accept-language'][1];
    }

    public function getAcceptDatetime()
    {
        return $this->headers['accept-datetime'][1];
    }

    public function getAuthorization()
    {
        return $this->headers['authorization'][1];
    }

    public function getCacheControl()
    {
        return $this->headers['cache-control'][1];
    }

    public function getConnection()
    {
        return $this->headers['connection'][1];
    }

    public function getCookie()
    {
        return $this->headers['cookie'][1];
    }

    public function getContentLength()
    {
        return $this->headers['content-length'][1];
    }

    public function getContentMd5()
    {
        return $this->headers['content-md5'][1];
    }

    public function getContentType()
    {
        return $this->headers['content-type'][1];
    }

    public function getDate()
    {
        return $this->headers['date'][1];
    }

    public function getExpect()
    {
        return $this->headers['expect'][1];
    }

    public function getFrom()
    {
        return $this->headers['from'][1];
    }

    public function getHost()
    {
        return $this->headers['host'][1];
    }

    public function getIfMatch()
    {
        return $this->headers['if-match'][1];
    }

    public function getIfModifiedSince()
    {
        return $this->headers['if-modified-since'][1];
    }

    public function getIfNoneMatch()
    {
        return $this->headers['if-none-match'][1];
    }

    public function getIfRange()
    {
        return $this->headers['if-range'][1];
    }

    public function getIfUnmodifiedSince()
    {
        return $this->headers['if-unmodified-since'][1];
    }

    public function getMaxForwards()
    {
        return $this->headers['max-forwards'][1];
    }

    public function getOrigin()
    {
        return $this->headers['origin'][1];
    }

    public function getPragma()
    {
        return $this->headers['pragma'][1];
    }

    public function getProxyAuthorization()
    {
        return $this->headers['proxy-authorization'][1];
    }

    public function getRange()
    {
        return $this->headers['range'][1];
    }

    public function getReferer()
    {
        return $this->headers['referer'][1];
    }

    public function getTe()
    {
        return $this->headers['te'][1];
    }

    public function getUserAgent()
    {
        return $this->headers['user-agent'][1];
    }

    public function getUpgrade()
    {
        return $this->headers['upgrade'][1];
    }

    public function getVia()
    {
        return $this->headers['via'][1];
    }

    public function getWarning()
    {
        return $this->headers['warning'][1];
    }

    public function getXRequestedWith()
    {
        return $this->headers['x-requested-with'][1];
    }

    public function getDnt()
    {
        return $this->headers['dnt'][1];
    }

    public function getXForwardedFor()
    {
        return $this->headers['x-forwarded-for'][1];
    }

    public function getXForwardedHost()
    {
        return $this->headers['x-forwarded-host'][1];
    }

    public function getXForwardedProto()
    {
        return $this->headers['x-forwarded-proto'][1];
    }

    public function getXHttpMethodOverride()
    {
        return $this->headers['x-http-method-override'][1];
    }

    public function getXWapProfile()
    {
        return $this->headers['x-wap-profile'][1];
    }

    public function getProxyConnection()
    {
        return $this->headers['proxy-connection'][1];
    }

    public function getXCsrfToken()
    {
        return $this->headers['x-csrf-token'][1];
    }

    public function setAccept($accept)
    {
        $header = $this->headers['accept'];
        $header[1] = $accept;
        unset($this->headers['accept']);
        $this->headers['accept'] = $header;
        return $this;
    }

    public function setAcceptCharset($acceptCharset)
    {
        $header = $this->headers['accept-charset'];
        $header[1] = $acceptCharset;
        unset($this->headers['accept-charset']);
        $this->headers['accept-charset'] = $header;
        return $this;
    }

    public function setAcceptEncoding($acceptEncoding)
    {
        $header = $this->headers['accept-encoding'];
        $header[1] = $acceptEncoding;
        unset($this->headers['accept-encoding']);
        $this->headers['accept-encoding'] = $header;
        return $this;
    }

    public function setAcceptLanguage($acceptLanguage)
    {
        $header = $this->headers['accept-language'];
        $header[1] = $acceptLanguage;
        unset($this->headers['accept-language']);
        $this->headers['accept-language'] = $header;
        return $this;
    }

    public function setAcceptDatetime($acceptDatetime)
    {
        $header = $this->headers['accept-datetime'];
        $header[1] = $acceptDatetime;
        unset($this->headers['accept-datetime']);
        $this->headers['accept-datetime'] = $header;
        return $this;
    }

    public function setAuthorization($authorization)
    {
        $header = $this->headers['authorization'];
        $header[1] = $authorization;
        unset($this->headers['authorization']);
        $this->headers['authorization'] = $header;
        return $this;
    }

    public function setCacheControl($cacheControl)
    {
        $header = $this->headers['cache-control'];
        $header[1] = $cacheControl;
        unset($this->headers['cache-control']);
        $this->headers['cache-control'] = $header;
        return $this;
    }

    public function setConnection($connection)
    {
        $header = $this->headers['connection'];
        $header[1] = $connection;
        unset($this->headers['connection']);
        $this->headers['connection'] = $header;
        return $this;
    }

    public function setCookie($cookie)
    {
        $header = $this->headers['cookie'];
        $header[1] = $cookie;
        unset($this->headers['cookie']);
        $this->headers['cookie'] = $header;
        return $this;
    }

    public function setContentLength($contentLength)
    {
        $header = $this->headers['content-length'];
        $header[1] = $contentLength;
        unset($this->headers['content-length']);
        $this->headers['content-length'] = $header;
        return $this;
    }

    public function setContentMd5($contentMd5)
    {
        $header = $this->headers['content-md5'];
        $header[1] = $contentMd5;
        unset($this->headers['content-md5']);
        $this->headers['content-md5'] = $header;
        return $this;
    }

    public function setContentType($contentType)
    {
        $header = $this->headers['content-type'];
        $header[1] = $contentType;
        unset($this->headers['content-type']);
        $this->headers['content-type'] = $header;
        return $this;
    }

    public function setDate($date)
    {
        $header = $this->headers['date'];
        $header[1] = $date;
        unset($this->headers['date']);
        $this->headers['date'] = $header;
        return $this;
    }

    public function setExpect($expect)
    {
        $header = $this->headers['expect'];
        $header[1] = $expect;
        unset($this->headers['expect']);
        $this->headers['expect'] = $header;
        return $this;
    }

    public function setFrom($from)
    {
        $header = $this->headers['from'];
        $header[1] = $from;
        unset($this->headers['from']);
        $this->headers['from'] = $header;
        return $this;
    }

    public function setHost($host)
    {
        $header = $this->headers['host'];
        $header[1] = $host;
        unset($this->headers['host']);
        $this->headers['host'] = $header;
        return $this;
    }

    public function setIfMatch($ifMatch)
    {
        $header = $this->headers['if-match'];
        $header[1] = $ifMatch;
        unset($this->headers['if-match']);
        $this->headers['if-match'] = $header;
        return $this;
    }

    public function setIfModifiedSince($ifModifiedSince)
    {
        $header = $this->headers['if-modified-since'];
        $header[1] = $ifModifiedSince;
        unset($this->headers['if-modified-since']);
        $this->headers['if-modified-since'] = $header;
        return $this;
    }

    public function setIfNoneMatch($ifNoneMatch)
    {
        $header = $this->headers['if-none-match'];
        $header[1] = $ifNoneMatch;
        unset($this->headers['if-none-match']);
        $this->headers['if-none-match'] = $header;
        return $this;
    }

    public function setIfRange($ifRange)
    {
        $header = $this->headers['if-range'];
        $header[1] = $ifRange;
        unset($this->headers['if-range']);
        $this->headers['if-range'] = $header;
        return $this;
    }

    public function setIfUnmodifiedSince($ifUnmodifiedSince)
    {
        $header = $this->headers['if-unmodified-since'];
        $header[1] = $ifUnmodifiedSince;
        unset($this->headers['if-unmodified-since']);
        $this->headers['if-unmodified-since'] = $header;
        return $this;
    }

    public function setMaxForwards($maxForwards)
    {
        $header = $this->headers['max-forwards'];
        $header[1] = $maxForwards;
        unset($this->headers['max-forwards']);
        $this->headers['max-forwards'] = $header;
        return $this;
    }

    public function setOrigin($origin)
    {
        $header = $this->headers['origin'];
        $header[1] = $origin;
        unset($this->headers['origin']);
        $this->headers['origin'] = $header;
        return $this;
    }

    public function setPragma($pragma)
    {
        $header = $this->headers['pragma'];
        $header[1] = $pragma;
        unset($this->headers['pragma']);
        $this->headers['pragma'] = $header;
        return $this;
    }

    public function setProxyAuthorization($proxyAuthorization)
    {
        $header = $this->headers['proxy-authorization'];
        $header[1] = $proxyAuthorization;
        unset($this->headers['proxy-authorization']);
        $this->headers['proxy-authorization'] = $header;
        return $this;
    }

    public function setRange($range)
    {
        $header = $this->headers['range'];
        $header[1] = $range;
        unset($this->headers['range']);
        $this->headers['range'] = $header;
        return $this;
    }

    public function setReferer($referer)
    {
        $header = $this->headers['referer'];
        $header[1] = $referer;
        unset($this->headers['referer']);
        $this->headers['referer'] = $header;
        return $this;
    }

    public function setTe($te)
    {
        $header = $this->headers['te'];
        $header[1] = $te;
        unset($this->headers['te']);
        $this->headers['te'] = $header;
        return $this;
    }

    public function setUserAgent($userAgent)
    {
        $header = $this->headers['user-agent'];
        $header[1] = $userAgent;
        unset($this->headers['user-agent']);
        $this->headers['user-agent'] = $header;
        return $this;
    }

    public function setUpgrade($upgrade)
    {
        $header = $this->headers['upgrade'];
        $header[1] = $upgrade;
        unset($this->headers['upgrade']);
        $this->headers['upgrade'] = $header;
        return $this;
    }

    public function setVia($via)
    {
        $header = $this->headers['via'];
        $header[1] = $via;
        unset($this->headers['via']);
        $this->headers['via'] = $header;
        return $this;
    }

    public function setWarning($warning)
    {
        $header = $this->headers['warning'];
        $header[1] = $warning;
        unset($this->headers['warning']);
        $this->headers['warning'] = $header;
        return $this;
    }

    public function setXRequestedWith($xRequestedWith)
    {
        $header = $this->headers['x-requested-with'];
        $header[1] = $xRequestedWith;
        unset($this->headers['x-requested-with']);
        $this->headers['x-requested-with'] = $header;
        return $this;
    }

    public function setDnt($dnt)
    {
        $header = $this->headers['dnt'];
        $header[1] = $dnt;
        unset($this->headers['dnt']);
        $this->headers['dnt'] = $header;
        return $this;
    }

    public function setXForwardedFor($xForwardedFor)
    {
        $header = $this->headers['x-forwarded-for'];
        $header[1] = $xForwardedFor;
        unset($this->headers['x-forwarded-for']);
        $this->headers['x-forwarded-for'] = $header;
        return $this;
    }

    public function setXForwardedHost($xForwardedHost)
    {
        $header = $this->headers['x-forwarded-host'];
        $header[1] = $xForwardedHost;
        unset($this->headers['x-forwarded-host']);
        $this->headers['x-forwarded-host'] = $header;
        return $this;
    }

    public function setXForwardedProto($xForwardedProto)
    {
        $header = $this->headers['x-forwarded-proto'];
        $header[1] = $xForwardedProto;
        unset($this->headers['x-forwarded-proto']);
        $this->headers['x-forwarded-proto'] = $header;
        return $this;
    }

    public function setXHttpMethodOverride($xHttpMethodOverride)
    {
        $header = $this->headers['x-http-method-override'];
        $header[1] = $xHttpMethodOverride;
        unset($this->headers['x-http-method-override']);
        $this->headers['x-http-method-override'] = $header;
        return $this;
    }

    public function setXWapProfile($xWapProfile)
    {
        $header = $this->headers['x-wap-profile'];
        $header[1] = $xWapProfile;
        unset($this->headers['x-wap-profile']);
        $this->headers['x-wap-profile'] = $header;
        return $this;
    }

    public function setProxyConnection($proxyConnection)
    {
        $header = $this->headers['proxy-connection'];
        $header[1] = $proxyConnection;
        unset($this->headers['proxy-connection']);
        $this->headers['proxy-connection'] = $header;
        return $this;
    }

    public function setXCsrfToken($xCsrfToken)
    {
        $header = $this->headers['x-csrf-token'];
        $header[1] = $xCsrfToken;
        unset($this->headers['x-csrf-token']);
        $this->headers['x-csrf-token'] = $header;
        return $this;
    }
}
