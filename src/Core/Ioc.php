<?php

namespace Claes\Core;

use Claes\Exception\IocException;

/**
 * Inversion of control class
 */
class Ioc
{
    /**
     * @var 
     */
    protected static $registry = [];

    /**
     * Add a new resolver to the registry array.
     * @param  string $name Identifier
     * @param  Closure $resolve Closure that creates instance
     */
    public static function register($name, \Closure $resolve)
    {
        static::$registry[$name] = $resolve;
    }

    /**
     * Get an instance
     * @param  string $name Identifier
     * @return mixed
     * @throws IocException
     */
    public static function resolve($name, ...$args)
    {
        if (static::isRegistered($name)) {
            $function = static::$registry[$name];
            return $function(...$args);
        }

        throw new IocException('Nothing registered with the name “'.$name.'”.');
    }

    /**
     * Determine whether the id is registered
     * @param  string $name The id
     * @return bool Whether the id exists or not
     */
    public static function isRegistered($name)
    {
        return array_key_exists($name, static::$registry);
    }
}