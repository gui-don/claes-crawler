<?php

namespace Claes\Core;

use Claes\Core\Interfaces\ContainerInterface;
use Claes\Core\Ioc;

/**
 * Default container of services for Claes crawler - Feel free to create your own
 */
class Container implements ContainerInterface
{
    public function registerRequestHeader($httpRequestHeaderClassName)
    {
        $self = $this;

        Ioc::register($httpRequestHeaderClassName, function() use ($httpRequestHeaderClassName) {
            return new $httpRequestHeaderClassName();
        });
    }
    /**
     * {@inheritdoc}
     */
    public function registerRequest($httpRequestClassName, $httpRequestHeaderClassName, $httpEngineClassName)
    {
        $self = $this;

        Ioc::register('request', function($url) use ($httpRequestClassName, $httpRequestHeaderClassName, $httpEngineClassName, &$self) {
            return new $httpRequestClassName($self->get($httpRequestHeaderClassName), new $httpEngineClassName($url), $self);
        });
    }

    /**
     * {@inheritdoc}
     */
    public function registerResponse($httpResponseClassName, $httpResponseHeaderClassName)
    {
        Ioc::register('response', function($content, $code, $rawHeaders) use ($httpResponseClassName, $httpResponseHeaderClassName) {
            return new $httpResponseClassName($content, $code, $rawHeaders, new $httpResponseHeaderClassName());
        });
    }

    /**
     * {@inheritdoc}
     */
    public function registerDomParser($domParserClassName)
    {
        Ioc::register('domparser', function($rawBody) use ($domParserClassName) {
            return new $domParserClassName($rawBody);
        });
    }

    public function get(string $className)
    {
        return Ioc::resolve($className);
    }

    /**
     * {@inheritdoc}
     */
    public function getRequest($url)
    {
        return Ioc::resolve('request', $url);
    }

    /**
     * {@inheritdoc}
     */
    public function getResponse($content, $code, $rawHeaders)
    {
        return Ioc::resolve('response', $content, $code, $rawHeaders);
    }

    /**
     * {@inheritdoc}
     */
    public function getDomParser($rawBody)
    {
        return Ioc::resolve('domparser', $rawBody);
    }
}