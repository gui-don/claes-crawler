<?php

namespace Claes\Core\Interfaces;

use Claes\Crawler\Interfaces\HttpResponseInterface;
use Claes\Crawler\Interfaces\HttpRequestInterface;
use Claes\Crawler\Interfaces\DomParserInterface;

interface ContainerInterface
{
    /**
     * Create an HttpResponse service
     */
    public function registerResponse($httpResponseClassName, $httpResponseHeaderClassName);

    /**
     * Create an HttpRequest service
     */
    public function registerRequest($httpRequestClassName, $httpRequestHeaderClassName, $httpEngineClassName);

    /**
     * Create an DomParser service
     */
    public function registerDomParser($domParserClassName);

    /**
     * Get an instance of a HTTP response type
     * @param string $content Response content
     * @param int $code Response code
     * @param string $rawHeaders Response raw headers
     * @return HttpResponseInterface
     */
    public function getResponse($content, $code, $rawHeaders);

    /**
     * Get an instance of a HTTP request type
     * @param string $url Url
     * @return HttpRequestInterface
     */
    public function getRequest($url);

    /**
     * Get an instance of a DOM Parser
     * @param string $rawBody Raw DOM
     * @return DomParserInterface
     */
    public function getDomParser($rawBody);
}