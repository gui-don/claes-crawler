# Claes Crawler [![pipeline status](https://gitlab.com/gui-don/claes-crawler/badges/master/pipeline.svg)](https://gitlab.com/gui-don/claes-crawler/commits/master)

---

<div dir="ltr" class="center">
    <p dir="ltr" align="center"><img width="153" height="146" src="claes.png" /></p>
</div>

| PHP 7.4 | PHP 7.3 | PHP 7.2 | PHP 7.1 | Older PHP |
|:-------:|:-------:|:-------:|:-------:|:---------:|
| ✔       | ✔       | ✔       | ✔       | ✖         |

## A HTTP Crawler ##

This crawler aims to disguise itself as a “true” web browser (Firefox, Chrome, etc.) to bypass protections.

### Installation ###

##### Composer:

    composer require gui-don/claes-crawler

##### Manual:

Import the `autoload.php` file in your code:

```php
require_once('autoload.php');

use \Claes\Client\HttpBrowser;

// etc
```

### Documentation ###

#### Basic example ####

```php
    use \Claes\Client\HttpBrowser;

    $httpBrowser = new HttpBrowser();
    $httpBrowser->get('http://www.hondosackett.com/Fernando/GsG/V4Ch18P28.jpg');
    $httpBrowser->getResponse()->download('./');
```

#### Advanced exemple ####
```php
    use \Claes\Client\HttpBrowser;

    $httpBrowser = new HttpBrowser();

    $httpBrowser->setStrictMode(false);

    $httpBrowser->getRequest()->getHeaders()->setUserAgent('Mozilla/5.0 (Windows NT 6.1; rv:72.0) Gecko/20100101 Firefox/72.0');
    $httpBrowser->getRequest()->getHeaders()->setHost('en.wikipedia.org');

    $httpBrowser->getRequest()->setVerifySSL(false);

    $httpBrowser->get('https://91.198.174.192/wiki/Cognitive_dissonance');

    if ($httpBrowser->getResponse()->getCode() == 200) {
        var_dump($httpBrowser->getResponse()->getHeaders()->convert());
    } else {
        echo 'Cannot download! HTTP status code is '.$httpBrowser->getResponse()->getCode();
    }
```
